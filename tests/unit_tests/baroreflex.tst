//see http://en.wikipedia.org/wiki/Spectral_density#Energy_spectral_density
Tmax=3560;
freq_sampling=2;
f0=0.5;
// Time instants
t=0:1/freq_sampling:Tmax;N=size(t,'*');
n=round(N/2)
//output signal
sig1=[sin(2*%pi*f0*t(1:n)) sin(2*%pi*1.2*f0*t(n+1:$))] ;//+1d-6*rand(t); 
// Input signal
sig2=2*sig1;//+1d-6*rand(t);

R=[];
SWL=[5 9 ];//SWL=5;
for swl=SWL

  SL=[256:24:1024];//SL=256;
  for sl=SL
    clear options
    options.sectionlength=sl;
    options.sectionstep=sl/2; //50% overlaping
    options.smoothwindowlength=swl;
    options.minimalcoherence=0.5;
    frequency_bands=[f0 2*f0]/freq_sampling;
    section_window=window("hm",sl);
    [energy,dispersion,inout_gains,inout_coherences]=baroreflex([sig1;sig2],frequency_bands,options);
    energy=energy*freq_sampling;
    r=energy(:,1)./([sum((sig1(1:sl).*section_window)^2);sum((sig2(1:sl).*section_window)^2)]);
    //r=energy(:,1)./([sum((sig1(1:sl))^2);sum((sig2(1:sl))^2)]);
    R=[R r(1)];
    mprintf("Tmax=%d,sectionlength=%d,sectionstep=%d,swl=%d,r=%f\n",...
            Tmax,options.sectionlength, options.sectionstep, ...
            options.smoothwindowlength,r(1))
  end
end

