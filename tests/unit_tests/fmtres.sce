CurPath=get_absolute_file_path("fmtres.sce");
load(CurPath+'testSPWDpars3.dat')
Res=res;
Cf=["f_fmin","f_lpwl","f_twl","f_fwl","f_nf",...
   "fmin/ef","lpwl/ef","twl/ef","fwl/ef","nf/ef",...
   "f_min","f_mean","f_std"];
Ca=["a_fmin","a_lpwl","a_twl","a_fwl","a_nf",...
   "fmin/a","lpwl/ea","twl/ea","fwl/ea","nf/ea",...
   "a_min","a_mean","a_std"];

i=2;
  res=Res(find(Res(:,1)==i),:);res=res(:,2:$);

  ef=res(:,6);
  ea=res(:,7);
  [mf,kf]=min(ef);
  [ma,ka]=min(ea);
  
  df=res(kf,1:5);
  for l=1:5,df=[df corr(ef,res(:,l),1)];end
  df=[df mf,mean(ef),stdev(ef)];
  da=res(ka,1:5);
  for l=1:5,da=[da corr(ea,res(:,l),1)];end
  da=[da ma,mean(ea),stdev(ea)];
  C=[Cf;string(df);Ca;string(da)];
  C=strsubst(C,'.',',');
  C=strcat(C,";","r")';
  mputl(C,"foo"+string(i)+".csv")
