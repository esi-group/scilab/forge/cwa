mode(-1)
// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

mode(-1)
subdemopath = get_absolute_file_path("CV_CDM_SPWVD.dem.gateway.sce");
subdemolist = [
    "RR and Vt available" "RR_Vt_CDM_SPWVD.dem.gateway.sce"
    "Vt not available" "RR_CDM_SPWVD.dem.gateway.sce"];
subdemolist(:,2) = subdemopath + subdemolist(:,2);
clear subdemopath 
