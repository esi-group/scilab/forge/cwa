// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



//test with 
//  - Piecewise constant amplitude
//  - sinusoidal modulation frequency
//---------------------------------

//Create a frequency modulated signal
f0=11;Tmax=10;f1=f0/40;
freq_sampling=f0*55;
fw=0.3; 

// Time instants
t=0:1/freq_sampling:Tmax;N=size(t,'*');

// Amplitude modulation
n1=round(N/3);
A=ones(1,n1);A(n1+1:2*n1)=2;A($+1:N)=1; 

// Frequency modulation
ft=(f0+fw*sin(2*%pi*f1*t)).*t
phi_ref=2*%pi*fw*sin(2*%pi*f1*t).*t;
// Input signal
sig=A.*cos(2*%pi*ft); 
//IFreq_ref:=diff(ft,t)
IFreq_ref=2*fw*cos(2*%pi*f1*t)*%pi*f1.*t+f0+fw*sin(2*%pi*f1*t)
// Frequency reference given by a signal
f_ref=cos(2*%pi*f0*t);

BPdelay=0; //no bandpass filter used

// Call the complex demodulation
[IFreq,IAmp,IPhase,delay,f0e]=ComplexDemodulation(sig,f_ref);
// Take sampling frequency into account
IFreq=IFreq*freq_sampling;
f0e=f0e*freq_sampling;


// Draw results
f=scf(100001);clf;f.figure_name="CDM";
demo_viewCode('CDM_sin.sce');

t=(0:size(IFreq,'*')-1)/freq_sampling;
timebounds=[t(BPdelay+delay);t($)];

d1=BPdelay/freq_sampling;
d2=delay/freq_sampling;

// Given signal
subplot(311);
plot(d1+d2+t(1:$-delay),sig,"m",t,IAmp,"b");
a=gca();
a.data_bounds(:,2)=[-3.5;3];
a.data_bounds(:,1)=timebounds;
a.tight_limits="on";
a.grid(1:2)=color("gray");
ylabel("signal & IAmp")
legend(["Signal","IAmp"],"in_lower_left");

// Instantaneous frequency
subplot(312);

plot(t,IFreq,d1+d2+t(1:$-delay),IFreq_ref,'m');


a=gca();a.data_bounds(:,2)=[5;15];
a.data_bounds(:,1)=timebounds;
a.tight_limits="on";

ylabel("IF (Hz)")
a.grid(1:2)=color("gray");
legend(["CDM","Real"],"in_lower_left");

// Instantaneous phase
subplot(313);
plot(t,IPhase,d1+d2+t(1:$-delay),phi_ref,'m');
a=gca();
a.data_bounds(:,1)=timebounds;
a.tight_limits="on";

ylabel("Phase shift(rd)")
a.grid(1:2)=color("gray");
legend(["CDM","Real"],"in_lower_left");
