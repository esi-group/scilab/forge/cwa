// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//patient with controled ergocycle
//manipRocq/10b.4Hz
scriptfilename = "CDM_SPWVD_handgrip.sce";
datapath   = get_absolute_file_path(scriptfilename);
datafile   = datapath+"DATA/handgrip_spont";
Title      = "Handgrip";

d=read(datafile,-1,7);
freq_sampling=4;
RR=d(:,1);


//narrow band filter
filterlength=355;
frequency_bounds=[0.45 0.7];

f=scf(100001);clf;f.figure_name=Title;
res=CDM_SPWVD_Analysis(RR,0.55,freq_sampling,frequency_bounds,filterlength) 
PlotSpectralAnalysis(res, _("RR"),Title)

demo_viewCode(scriptfilename);


