// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//lying patient with spontaneous breathing, and then  breathing controled
//at 0.25Hz, finally after tilt breathing controled at 0.15 Hz
//TILT/BALEX_2Hz
scriptfilename = "CDM_SPWVD_tilt.sce";
datapath   = get_absolute_file_path(scriptfilename);
datafile   = datapath+"DATA/tilt_divers";
Title      = "Tilt BALEX_2Hz";

d=read(datafile,-1,5);
freq_sampling=2;
RR=d(:,1);
Vt=d(:,4);

//narrow band filter
filterlength=355;
frequency_bounds=[0.12 0.3];

f=scf(100001);clf;f.figure_name=Title;
res=CDM_SPWVD_Analysis(RR,Vt,freq_sampling,frequency_bounds,filterlength) 
PlotSpectralAnalysis(res,[_("RR"),_("Vt")],Title)
messagebox(_("Click Ok to add SBP signal"),"modal")
AddChannelPlot(d(:,2),"SBP")

messagebox([_("Use ""Scroll"" menu to navigate over time");
            _("with directional arrows")])

demo_viewCode(scriptfilename);
