mode(-1)
// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function subdemolist = demo_gateway()

  demopath = get_absolute_file_path("Cardiovascular.dem.gateway.sce");
  subdemolist = [
      "Basic tools"                  "BasicTools.dem.gateway.sce"
      "Cardio Vascular applications" "CardiovascularApplications.dem.gateway.sce" ];
  subdemolist(:,2) = demopath + subdemolist(:,2);
  
endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
