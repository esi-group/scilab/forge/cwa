// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//free breathing , sane patient
//CottinPB/Fran_spon_4Hz
scriptfilename = "CDM_SPWVD_fb_sp.sce";
datapath   = get_absolute_file_path(scriptfilename);
datafile   = datapath+"DATA/decubitus_spont";
Title      = "Fran_spon_3s_4Hz";

d=read(datafile,-1,4);
freq_sampling=4;
RR=d(:,1);
Vt=d(:,3);

//narrow band filter
filterlength=355;
frequency_bounds=[0.25 0.35];
f=scf(100001);clf;f.figure_name=Title;
res=CDM_SPWVD_Analysis(RR,Vt,freq_sampling,frequency_bounds,filterlength) 
PlotSpectralAnalysis(res,[_("RR"),_("Vt")],Title)

demo_viewCode(scriptfilename);

