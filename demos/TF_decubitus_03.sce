// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

scriptfilename = "TF_decubitus_03.sce";
datapath   = get_absolute_file_path(scriptfilename);
datafile   = datapath+"DATA/decubitus_03";
Title      = "CDM decubitus_03";

d=read(datafile,-1,4)';
freq_sampling=4;
RR=d(1,:);//variation de la duree entre 2 battements
Vt=d(3,:);//respiration, utilisée pour déterminer la fréquence de base f0 A REVOIR


//narrow band filter
f1=scf(100001);clf;f1.figure_position=[0 0];f1.figure_size=[524 514];
f1.figure_name="TimeFrequency RR decubitus_03";
opt=struct();opt.method="spwv"
TimeFrequencyTool(RR,freq_sampling,opt) 

f2=scf(100002);clf;f2.figure_position=[450 0];f2.figure_size=[524 514];
f2.figure_name="TimeFrequency Vt decubitus_03";
TimeFrequencyTool(Vt,freq_sampling,opt) 

