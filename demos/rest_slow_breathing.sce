// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//test with cardiovalscular RR signal
scriptfilename = "rest_slow_breathing.sce";
datapath   = get_absolute_file_path(scriptfilename);

datafile=datapath+"DATA/rest_slow_breathing_4Hz";
Title      = "rest_slow_breathing";
d=read(datafile,-1,4)';

freq_sampling=4;
shift=1;
RR= d(1,shift:$);
SBP=d(2,shift:$);


//options.minimalcoherence=0.7;
fbands=[0.1 0.2; 0.04 0.08];
//fbands=[0.2 0.3];
//fbands=[0.04 0.15];
clear options
options.sectionlength=64;//ceil(freq_sampling/min(fbands));
options.sectionlength=128
BPfbounds=[0.02 0.4];
BPflen=255;
result=Baroreflex_Analysis(RR,SBP,freq_sampling,fbands,BPfbounds,BPflen,options)
f=scf(100001);clf;f.figure_name=Title;
PlotBaroreflexAnalysis(result,Title)
demo_viewCode(scriptfilename); 

