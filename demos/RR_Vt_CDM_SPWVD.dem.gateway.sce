mode(-1)
// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

mode(-1)
subdemopath = get_absolute_file_path("RR_Vt_CDM_SPWVD.dem.gateway.sce");
subdemolist = [
    "Tilt test (RR+Vt)" "CDM_SPWVD_tilt.sce"
    "decubitus_03  (RR+Vt)" "CDM_SPWVD_decubitus_03.sce"
    "Controlled breathing, control subject (RR+Vt)" "CDM_SPWVD_cb_sp.sce"
    "Controlled breathing, intensive care patient (RR+Vt)" "CDM_SPWVD_cb_icup.sce"
    "Free breathing, control subject (RR+Vt)" "CDM_SPWVD_fb_sp.sce"
    "Apnea test (RR+Vt)" "CDM_SPWVD_apnea.sce"];
subdemolist(:,2) = subdemopath + subdemolist(:,2);
clear subdemopath 
