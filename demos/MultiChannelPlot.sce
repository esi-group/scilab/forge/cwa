// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
scf(10001)
t=linspace(0,500,5000)';
opt=struct();opt.title="MultiChannelPlot";
Ax=MultiChannelPlot(t,sin(t),"sin(t)",[sin(2*t) sin(3*t)],"y",sin(t)^3,"sin(t)^3",opt);
legend(Ax(2),["sin(2*t)","sin(3*t)"]);
messagebox(_("Click ok to continue"),"modal")

ax=AddChannelPlot(sin(0.1*t),"sin(5*t)");
legend(ax,"sin(5*t)");
messagebox([_("After ""Zoom"" use ""Scroll"" menu to navigate over time");
            _("with directional arrows")])

demo_viewCode('MultiChannelPlot.sce');
