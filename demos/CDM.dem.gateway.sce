// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

mode(-1)
subdemopath = get_absolute_file_path("CDM.dem.gateway.sce");
subdemolist = [
    "Sinusoidal modulated frequency, signal for reference" "CDM_sin.sce"
    "Linear modulated frequency, signal for reference" "CDM_lin.sce"
    "Linear modulated frequency, contant frequency reference" "CDM_lin1.sce" ];
subdemolist(:,2) = subdemopath + subdemolist(:,2);
clear subdemopath
