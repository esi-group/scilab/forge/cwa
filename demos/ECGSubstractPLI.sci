function S=ECGSubstractPLI(S,fe,f,Threshold)
//This function remoces power-line interference from the ECG using
//substraction procedure:
//  Sf=ECGsubstractPLI(S,fe,f,threshold)
  
//references :
//- "Substraction of 50Hz interference from the electrocardiogram" C.levkov,
//  G. Micov, R.Ivanov and I. K. Daskalov
//  Medical and Biological Engineering and Computing
//  July 1984, Volume 22, Issue 4, pp 371-373
//  http://link.springer.com/article/10.1007%2FBF02442109#page-1
//
//- "Removal of power-line interference from the ECG: a review of the
//  subtraction procedure"  
//  Chavdar Levkov, Georgy Mihov, Ratcho Ivanov,
//  Ivan Daskalov, Ivaylo Christovand Ivan Dotsinsky
//  BioMedical Engineering OnLine  2005,4:50 
//  http://www.biomedical-engineering-online.com/content/pdf/1475-925X-4-50.pdf
  
  N=fe/f;//number of samples for an interference period
  if N<>round(N) then
    error("Sampling frequency must be a multiple of the PLI frequency")
  end
  
  if or(size(S)==1) then
    S=S(:)
    Nchannels=1
  else
    Nchannels=size(S,2);
  end
  Nsamples=size(S,1);

  if argn(2)<4 then 
    Threshold=(max(S,'r')-min(S,'r'))/20;
  else
    if type(Threshold)<>1|~isreal(Threshold) then
      error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),"ECGSubstractPLI",4))
    end
    if size(Threshold,'*')==1 then
      Threshold=Threshold*ones(Nchannels,1)
    elseif size(Threshold,'*')<>Nchannels then
       error(msprintf(_("%s: incompatible input arguments %d and %d\n"),"ECGSubstractPLI",1,4))
    end
  end
  for k=1:Nchannels
    S(:,k)=Levkov(S(:,k),fe,f,Threshold(k));
  end
endfunction

