mode(-1)
// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

mode(-1)
subdemopath = get_absolute_file_path("RR_CDM_SPWVD.dem.gateway.sce");
subdemolist = [
    "Ergocycle test (RR)" "CDM_SPWVD_ergocycle.sce"
    "Handgrip test (RR)" "CDM_SPWVD_handgrip.sce"
    "Controlled breathing, control subject (RR)" "CDM_SPWVD_cb_sp_RR.sce"];
subdemolist(:,2) = subdemopath + subdemolist(:,2);
clear subdemopath 
