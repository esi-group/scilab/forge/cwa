// This file is part of the CardioVascular toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//test with 
//  - Piecewise constant amplitude
//  - linearly modulated frequency
//---------------------------------

// Create a frequency modulated signal
f0=11;Tmax=10;
freq_sampling=f0*55;
fw=0.5;

// Time instants
t=0:1/freq_sampling:Tmax;N=size(t,'*');

// Frequency modulation
IFreq_ref=f0+linspace(-fw,fw,N);

// Amplitude modulation
n1=round(N/3);
A=ones(1,n1);A(n1+1:2*n1)=2;A($+1:N)=1; 




// Input signal
sig=A.*cos(2*%pi*IFreq_ref.*t);
BPdelay=0; //no bandpass filter used

// Call the SPWVD demodulation
[T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig);

// Take sampling frequency into account
IFreq=IFreq*freq_sampling; 

// Draw results
f=scf(100001);clf;f.figure_name="SPWVD";

t=(0:size(IFreq,'*')-1)/freq_sampling;
timebounds=[t(BPdelay+delay);t($)];


d1=BPdelay/freq_sampling;

// Given signal
subplot(211);
plot(d1+t(delay+1:$),sig,"m",t,IAmp,"b");
a=gca();
a.data_bounds(:,1)=timebounds;
a.tight_limits="on";
a.grid(1:2)=color("gray");
ylabel("Signal&IAmp")

// Instantaneous frequency
subplot(212);
plot(t,IFreq,d1+t(delay+1:$),IFreq_ref,'r');
a=gca();
a.data_bounds(:,1)=timebounds;
a.tight_limits="on";

ylabel("IF (Hz)")
a.grid(1:2)=color("gray");
legend(["Data" "IF"],"in_lower_left");
demo_viewCode('SPWVD_lin.sce');
