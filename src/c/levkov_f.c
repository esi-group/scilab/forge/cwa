#include "machine.h"
#include "math.h"

extern void C2F(mget) (int *fd, double *res, int *n, char *type, int *ierr);
extern void C2F(mput) (int *fd, double *res, int *n, char *type, int *ierr);
int Levkov_f(int in,int out,int N,double *Threshold,int Nchannels, int Nsamples, double *work)
/* http://www.pudn.com/downloads95/sourcecode/math/biometrics/detail386898.html*/
{
  int i=0,ib=0,j=0,k=0,k1=0,l=0;
  int i0,i1,iNs2,iN,iNp1,ik;
  double sum=0.0;
  double d1,d2;
  int Ns2 = (N%2==0)? (N/2-1):((N-1)/2);
  int Np2 = N+2;
  int ierr =0;
  double *temp = work;
  double *src = temp+N*Nchannels;
  double *des = src+(N+2)*Nchannels;
  int *A = (int *)(des+Nchannels);

  for (l=0;l<Nchannels;l++) A[l]=1;
 
  /*initialization*/
  k=(N+2)*Nchannels;
  C2F(mget) (&in, src, &k, "d", &ierr);
  /*Copy first Ns2 src samples*/
  k=Ns2*Nchannels;
  C2F(mput) (&out, src, &k, "d", &ierr);

  /*adress within  circular buffer src*/
  i0=0;
  i1=1;
  iNs2=Ns2;
  iN=N;
  iNp1=N+1;
  for(k=0;k<N*Nchannels;k++) temp[k]=0.0;
  for (i=0;i<Nsamples-N-1;i++){
    for (l=0;l<Nchannels;l++) {
      d1=fabs(src[l+iN*Nchannels]-src[l+i0*Nchannels]);
      d2=fabs(src[l+iNp1*Nchannels]-src[l+i1*Nchannels]);
      if(fabs(d1-d2)<Threshold[l]) {/*linear interval detected*/
        A[l]--;
        if(A[l]==0){
          A[l]=1;
          ik=i0;
          for(k=0;k<N;k++) {
            sum+=src[l+ik*Nchannels];
            if (++ik>=Np2)  ik=0;
          }

          if(N%2==0) sum-=(src[l+iN*Nchannels]-src[l+i0])/2;
          des[l]=sum/N;
          temp[l+j*Nchannels]=src[l+iNs2*Nchannels]-des[l];
          sum=0.0;
        }
        else {
          des[l]=src[l+iNs2*Nchannels]-temp[l+j*Nchannels];
        }
      }
      else{ /*non linear part*/
        A[l]=N;
        des[l]=src[l+iNs2*Nchannels]-temp[l+j*Nchannels];
      }
    }
    /*Save current value of the filtred signal*/
    C2F(mput) (&out, des, &Nchannels, "d", &ierr);

    /*Shift src register left*/
    /*
    for (l=0;l<Nchannels;l++) {
      for(k=0;k<=N;k++) src[l+k*Nchannels]=src[l+(k+1)*Nchannels];
    }
    */
    /*Get a new signal entry*/
    C2F(mget) (&in, &src[i0*Nchannels], &Nchannels, "d", &ierr);
   
    /*update adresses in circular buffer*/
    if (++i0>=Np2)  i0=0;
    if (++i1>=Np2)  i1=0;
    if (++iNs2>=Np2) iNs2=0;
    if (++iN>=Np2) iN=0;
    if (++iNp1>=Np2) iNp1=0;
    if (++j>=N) j=0;
  }
 /*Copy  last src samples */ 
  k=(N-Ns2+1)*Nchannels;
  C2F(mput) (&out, &src[(Ns2+1)*Nchannels], &k, "d", &ierr);
  return 0;
}
