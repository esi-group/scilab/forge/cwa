#include <math.h>
void SampEn(double *y, int M, double r, int n, double *p,double *A,double *B,long *iw)
{
    long *run = iw ;
    long *lastrun = run+n;
    long  N;
    
    int M1, j, nj, jj, m;
    int i;
    double y1,s,st,rst;

    /* computation of the standard deviation of y */
    s=0;for (m=0;m<n;m++) s+=y[m];
    s=s/n;
    st=0;for (m=0;m<n;m++) st+=pow((y[m]-s),2.0);
    st=sqrt(st/(n-1));
    rst=r*st;

    for (m = 0; m < M; m++) {A[m]=0;B[m]=0;}
    for (m = 0; m < n; m++) {run[m]=0;lastrun[m]=0;}
    /* start running */
    for (i = 0; i < n - 1; i++) {
	nj = n - i - 1;
	y1 = y[i];
	for (jj = 0; jj < nj; jj++) {
	    j = jj + i + 1;
	    if (((y[j] - y1) < r) && ((y1 - y[j]) < rst)) {
		run[jj] = lastrun[jj] + 1;
		M1 = M < run[jj] ? M : run[jj];
		for (m = 0; m < M1; m++) {
		    A[m]++;
		    if (j < n - 1)
			B[m]++;
		}
	    }
	    else
		run[jj] = 0;
	}			/* for jj */
	for (j = 0; j < nj; j++)
	    lastrun[j] = run[j];
    }				/* for i */


    for (m = M-1; m >0; m--) B[m]=B[m-1];
    B[0]=(long) (n * (n - 1) / 2);

    for (m = 0; m<M; m++) {
      p[m] = -log(A[m] / B[m]);
    }

}
