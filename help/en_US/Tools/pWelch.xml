<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2014 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="pWelch">
  <refnamediv>
    <refname>pWelch</refname>
    <refpurpose>Power spectral density using Welch's method.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[PSD,F] = pWelch(x [,window_length [,overlap [,nf [,fs [,opt]]]]])</synopsis>
    <synopsis>[PSD,F] = pWelch(x ,window_length ,overlap ,F [,fs [,opt]]]]])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>x</term>
        <listitem>
          <para>
            A real 1D array of length N, the signal.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>window_length</term>
        <listitem>
          <para>
            A scalar with integer value, the length of the Hamming
            window. If omitted or set to [] the default value (N/8) is
            used.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>overlap</term>
        <listitem>
          <para>
             A scalar with integer value, the overlap amount in number
             samples.  If omitted or set to [] the default value
             (window_length/2) is used.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>nf</term>
        <listitem>
          <para>
             A scalar with integer value, the number of requested
             frequency values.  If omitted or set to [] the default
             value (max(256,2^nextpow2(window_length/2))) is used.
          </para>
        </listitem>
      </varlistentry>
       <varlistentry>
        <term>F</term>
        <listitem>
          <para>
             A real 1D array with positive values, the requested
             frequency values in HZ.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>fs</term>
        <listitem>
          <para>
            A positive scalar, the signal's sampling frequency in
            Hz. If omitted or or set to [] the default value (1) is
            used.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>opt</term>
        <listitem>
          <para>
            A character string with possible value: "twosided" or
            "onesided", this argument specifies if the returned PSD
            includes both parts of the spectrum or not. The default
            value is "onesided" unless F input argument has been
            specified.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>PSD</term>
        <listitem>
          <para>
            A  real column array, the power spectral estimate in s^2/Hz
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>F</term>
        <listitem>
          <para>
            A real  column array with positive values, the frequency values associated with  PSD.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      This function estimates the power spectral density of a signal
      using the non-parametric <ulink
      url="https://www.researchgate.net/publication/3445643_The_use_of_fast_Fourier_transform_for_the_estimation_of_power_spectra_A_method_based_on_time_averaging_over_short_modified_periodograms">Welch's
      method</ulink>.
    </para>
  </refsection>
  <refsection>
    <title>More information</title>
    <note><para>This function is intended to match the pwelch Matlab function</para></note>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
fe=1000;
t=0:1/fe:10;
x=0.25*sin(2*%pi*44*t) + sin(2*%pi*100*t) + rand(t);
[PSD,f] = pWelch(x,256,128,256,fe);
set(gcf(),"axes_size",[610,280]);
clf;plot(f,PSD)
xlabel("Frequency (Hz)")
ylabel("Power (s^2/Hz)")
    ]]></programlisting>
    <scilab:image><![CDATA[
fe=1000;
t=0:1/fe:10;
x=0.25*sin(2*%pi*44*t) + sin(2*%pi*100*t) + rand(t);
[PSD,f] = pWelch(x,256,128,256,fe);
set(gcf(),"axes_size",[610,280]);
clf;plot(f,PSD)
xlabel("Frequency (Hz)")
ylabel("Power (s^2/Hz)")
    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="scilab.help/pspect"  type="scilab">pspect</link>
      </member>
      <member>
        <link linkend="pBurg">pBurg</link>
      </member>

    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
         Welch, P.D. (1967) "The Use of Fast Fourier Transform for the
         Estimation of Power Spectra: A Method Based on Time Averaging
         Over Short, Modified Periodograms", IEEE Transactions on
         Audio Electroacoustics, AU-15, 70–73. The article can be
         found <ulink
         url="https://www.researchgate.net/publication/3445643_The_use_of_fast_Fourier_transform_for_the_estimation_of_power_spectra_A_method_based_on_time_averaging_over_short_modified_periodograms">here</ulink>
       </para>
       <ulink url="http://en.wikipedia.org/wiki/Welch%27s_method">http://en.wikipedia.org/wiki/Welch's_method</ulink>
       <para>
       </para>
     </refsection>
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function  added</revdescription>
        </revision>
      </revhistory>
    </refsection>
  <refsection>
     <title>Used Functions</title>
       <para>
         This function is based on the <link linkend="scilab.help/fft"  type="scilab">fft</link> function.
       </para>
     </refsection>
</refentry>
