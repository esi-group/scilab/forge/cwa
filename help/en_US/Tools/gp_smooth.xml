<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2013 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="gp_smooth">
  <refnamediv>
    <refname>gp_smooth</refname>
    <refpurpose>Smoothing by Gaussian process priors.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>ys = gp_smooth(x,y,fc)</synopsis>
    <synopsis>ys = gp_smooth(y,fc)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>x</term>
        <listitem>
          <para>
            A read 1D array of length N in increasing order, the data
            set abscissae. It may be not regularly sampled. If x is
            omitted it is supposed to be 1:N.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>y</term>
        <listitem>
          <para>
             A read 1D array of length N . the  data set ordinates
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>fc</term>
        <listitem>
          <para>
            The approximate normalized −3 dB cutoff frequency.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>ys</term>
        <listitem>
          <para>
            A read 1D array of length N. The regression curve ordinates.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      The Smoothing by Gaussian process Priors algorithm is a
      second-order response time-varying filter which operates on
      regularly or irregularly sampled data without compromising low frequency
      fidelity. It provides detrending  low-pass
      filtering with explicitly specified −3 dB cut-off points.
    </para>
    
  </refsection>
  <refsection>
    <title>More information</title>
    <note><para>For irregularly sampled data, a small problem is to
    assume a representative sampling frequency to establish the
    normalized cutoff frequency fc frequency interval. One can take for
    example the reciprocal of the median sampling interval of
    x.</para></note>
   
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
x=cumsum(rand(1,1000,'unif'));// randomly spaced abscissae
m=mean(x);sig=30;
y=exp(-(x-m).^2/(2*sig^2))+0.2*rand(x);

fs=1/mean(diff(x)); // representative sampling frequency 
ys1 = gp_smooth(x,y,0.05/fs);
ys2 = gp_smooth(x,y,0.005/fs);

clf;plot(x,y,"b",x,ys1,"r",x,ys2,"y")
e=gce();e.children(1:2).thickness=2;e.children(3).foreground=color("gray");
legend(["Noisy data";"fc="+string(0.05/fs);"fc="+string(0.005/fs)]);

    ]]></programlisting>
    <scilab:image><![CDATA[
x=cumsum(rand(1,1000,'unif'));// randomly spaced abscissae
m=mean(x);sig=30;
y=exp(-(x-m).^2/(2*sig^2))+0.2*rand(x);

fs=1/mean(diff(x)); // representative sampling frequency 
ys1 = gp_smooth(x,y,0.05/fs);
ys2 = gp_smooth(x,y,0.005/fs);

clf;plot(x,y,"b",x,ys1,"r",x,ys2,"y")
e=gce();e.children(1:2).thickness=2;e.children(3).foreground=color("gray");
legend(["Noisy data";"fc="+string(0.05/fs);"fc="+string(0.005/fs)]);
    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="loess">loess</link>
      </member>
      <member>
        <link linkend="sgolayfilt">sgolayfilt</link>
      </member>
      <member>
        <link linkend="polyfit">polyfit</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
         An Efficient Time-Varying Filter for Detrending and Bandwidth
         Limiting the Heart Rate Variability Tachogram without
         Resampling: MATLAB Open-Source Code and Internet Web-Based
         Implementation, A. Eleuteri, A. C. Fisher, D. Groves, and
         C. J. Dewhurst, Hindawi Publishing Corporation Computational
         and Mathematical Methods in Medicine Volume 2012, Article ID
         578785, (/http://downloads.hindawi.com/journals/cmmm/2012/578785.pdf)
       </para>
     </refsection>
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function  added</revdescription>
        </revision>
      </revhistory>
    </refsection>
  <refsection>
     <title>Used Functions</title>
       <para>
         This function uses <link linkend="scilab.help/spchol"
         type="scilab">spchol</link> to factorize spase definitive
         positive matrix for an efficient computation of the linear
         problem solution.
       </para>
     </refsection>
</refentry>
