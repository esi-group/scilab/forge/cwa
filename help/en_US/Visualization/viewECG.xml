<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2014 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="viewECG">
  <refnamediv>
    <refname>viewECG</refname>
    <refpurpose>Visualize a multi-channel ECG.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>viewECG(s,fs [,tit])</synopsis>
    <synopsis>viewECG(S [,tit])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>S</term>
        <listitem>
          <para>
            A multi-channel ECG given by a <link
            linkend="sciecg">sciecg</link> data structure.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
      <term>s</term>
        <listitem>
          <para>
            An array, each column contains the samples (in mV) of a channel.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>fs</term>
        <listitem>
          <para>
            A positive scalar: the sampling frequency in Hz.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>tit</term>
        <listitem>
          <para>
            A character string array, the title of the generated figure. The default value is [].
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      This function displays a multi-channel ECG in a graphic figure. Each channel is drawn in a subwindow.  
    </para>
    
  </refsection>
  <refsection>
    <title>More information</title>
    <tip><para>For long ECG signal, it should be better using <link linkend="ECGViewer">ECGViewer</link> which allows to interactively select parts.</para></tip>
   
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
S=readEcgsFile(CWApath()+"demos/datafiles/twa00.ecgs");
viewECG(S,"twa00")
    ]]></programlisting>
    <scilab:image><![CDATA[
set(gcf(),"axes_size",[610,390])
S=readEcgsFile(CWApath()+"demos/datafiles/twa00.ecgs");
viewECG(S,"twa00")
    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="ECGViewer">ECGViewer</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
  <refsection>
    <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function  added</revdescription>
        </revision>
      </revhistory>
    </refsection>
 
</refentry>
