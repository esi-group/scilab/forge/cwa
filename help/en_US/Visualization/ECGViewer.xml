<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
 Copyright (C) 2014 - INRIA - Serge Steer
 This file must be used under the terms of the CeCILL.
 This source file is licensed as described in the file COPYING, which
 you should have received as part of this distribution.  The terms
 are also available at
 http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="ECGViewer">
  <refnamediv>
    <refname>ECGViewer</refname>
    <refpurpose>Interactive tool for visualization of ECG and detections</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>ECGViewer(filein [,Loc])</synopsis>
    <synopsis>ECGViewer(ecg [,Loc])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>filein</term>
        <listitem>
          <para>
            a character string, the path of a Scilab ECG file (see  <link linkend="ecgs">ecgs</link>).
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>ecg</term>
        <listitem>
          <para>A multi channel <link linkend="sciecg">sciecg</link> data structure.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Loc</term>
        <listitem>
          <para>
            A 9 by N array of indexes, the beat event locations (see  <link linkend="ECGDetections">ECGDetections</link>).
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <itemizedlist>
      <listitem>
        <para>
          ECGViewer(filein) allows to display the signals contained in a
          Scilab ECG file on specified time intervals.
        </para>
        <para>
          The ECGViewer menu allows to specify the interval of
          interest by the index of the first sample ("Set start index"
          submenu) and the number of samples ("Set interval length"
          submenu) as well as the channel of interest ("Select
          channels"). The "Show" submenu generates the display
          according to the settings.
        </para>
        <para>It is possible to scroll view by a step equal to the
        interval length using the >> (scroll right) or &lt;&lt;(scroll
        left) menus. It is also to scroll view by a step equal to the
        half of the interval length using the > or &lt; menus.</para>
        <para>It is possible to use the standard zoom tool to examine
        parts of a current view.</para>
      </listitem>
      <listitem>
        <para>ECGViewer(filein, LOC) allows to display the signals
        contained in a Scilab ECG file together with the location of
        beats events (see <link
        linkend="ECGDetections">ECGDetections</link>) and also the
        evolution of selected beat segments like RpRp, QoQO, TpTe,
        QoTe ("Set options" submenu). </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection>
    <title>More information</title>
    <tip><para>The start index and interval length values specifies a
    number of samples, but it is possible to enter then in seconds, or
    minutes or hours using syntax like 10*s (10 seconds), 3.5*mn (3
    minutes and half), 1*h (one hour).</para></tip>
  </refsection>
  <refsection>
    <title>Examples</title>
    <itemizedlist>
      <listitem>
        <para>Visualization of a multi-channel ECG stored in a file</para>
        <itemizedlist>
          <listitem>
            <para>ECG only</para>
    <programlisting role="example"><![CDATA[
ECGViewer(CWApath()+"demos/datafiles/P5J0.ecgs")
]]></programlisting>
<para>With block length 60*s one obtains the following display</para>
    <scilab:image><![CDATA[
fig=scf(0);fig.axes_size=[616,300];
ECGViewer(CWApath()+"demos/datafiles/P5J0.ecgs")
ECGViewerMenus(_("Set interval length"),0,60*500)
ECGViewerMenus(_("Show"),0)
mclose(fig.user_data.fid)
]]></scilab:image>
<para>With block length set to 60*s,selected channel set to 1 and
start index set to 10*s one obtains the next display:</para>

<scilab:image><![CDATA[
fig=scf(0);fig.axes_size=[616,300];
ECGViewer(CWApath()+"demos/datafiles/P5J0.ecgs")
ECGViewerMenus(_("Select channels"),0,1)
ECGViewerMenus(_("Set start index"),0,10*500)
ECGViewerMenus(_("Show"),0)
mclose(fig.user_data.fid)
    ]]></scilab:image>
      </listitem>
      <listitem>
        <para>ECG with detections locations and segment length</para>
    <programlisting role="example"><![CDATA[
    ECGDetrendBatch(CWApath()+"demos/datafiles/P5J0.ecgs",TMPDIR+"/P5J0.ecgs")
    ECGSubstractPLIBatch(TMPDIR+"/P5J0.ecgs",50,0.07)
    SynthesisECGBatch(TMPDIR+"/P5J0_pli.ecgs")
    LOC=ECGDetectionsBatch(TMPDIR+"/P5J0_pli_l.ecgs");
    ECGViewer(TMPDIR+"/P5J0.ecgs",LOC)
    
]]></programlisting>
<para>With block length set to 10*s, start index set to 5*s, selected
detections set to [Rp,Tp,Pp] and selected segments set to [RpRp,TpTe]
one obtains the next display:</para>
<scilab:image><![CDATA[
ECGDetrendBatch(CWApath()+"demos/datafiles/P5J0.ecgs",TMPDIR+"/P5J0.ecgs")
ECGSubstractPLIBatch(TMPDIR+"/P5J0.ecgs",50,0.07)
SynthesisECGBatch(TMPDIR+"/P5J0_pli.ecgs")
LOC=ECGDetectionsBatch(TMPDIR+"/P5J0_pli_l.ecgs");

fig=scf(0);fig.axes_size=[616,500];
ECGViewer(TMPDIR+"/P5J0.ecgs",LOC)
ECGViewerMenus(_("Set interval length"),0,5*500)
ECGViewerMenus(_("Set start index"),0,10*500)
ECGViewerMenus(_("Set options"),0,0,[1 4 7],[3 4])

ECGViewerMenus(_("Show"),0)
mclose(fig.user_data.fid)
    ]]></scilab:image>
      </listitem>
        </itemizedlist>
 </listitem>
   
      <listitem>
        <para>Visualization of a multi-channel ECG stored in an ecg data structure</para>
<programlisting role="example"><![CDATA[
S=ECGDetrend(readEcgsFile(CWApath()+"demos/datafiles/P5J0.ecgs",1,50100));
LOC=ECGDetections(SynthesisECG(S));
ECGViewer(S,LOC)
ECGViewerMenus(_("Set options"),0,0,[1 4 7],[2 3 4])
ECGViewerMenus(_("Show"),0)
]]></programlisting>
<scilab:image><![CDATA[
S=ECGDetrend(readEcgsFile(CWApath()+"demos/datafiles/P5J0.ecgs",1,50100));
LOC=ECGDetections(SynthesisECG(S));

fig=scf(0);fig.axes_size=[616,500];clf();
ECGViewer(S,LOC)
ECGViewerMenus(_("Set options"),0,0,[1 4 7],[2 3 4])
ECGViewerMenus(_("Show"),0)
    ]]></scilab:image>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="ECGDetectionsBatch" >ECGDetectionsBatch</link>
      </member>
      <member>
        <link linkend="ecgs">ecgs</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
 
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function  added</revdescription>
        </revision>
      </revhistory>
    </refsection>
  
</refentry>
