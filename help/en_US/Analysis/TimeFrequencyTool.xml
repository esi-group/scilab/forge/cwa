<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2012 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xmlns:svg="http://www.w3.org/2000/svg"  
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML" 
          xmlns:db="http://docbook.org/ns/docbook" 
          xml:lang="en" xml:id="TimeFrequencyTool">
  <info>
    <pubdate>$LastChangedDate: 09-05-2012 $</pubdate>
  </info>
  <refnamediv>
    <refname>TimeFrequencyTool</refname>
    <refpurpose>Tool based on time frequency methods</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>TimeFrequencyTool(signal,freq_sampling,options)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>signal</term>
        <listitem>
          <para>
            a real vector, the signal to be analyzed.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>freq_sampling</term>
        <listitem>
          <para>
            a positive scalar, the sampling frequency of the signal.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>options</term>
        <listitem>
          <para>
            A struct with possible fields:
          </para>
          <itemizedlist>
            <listitem>
              <term>freq_step</term>, a scalar, the frequency resolution in Hz.
            </listitem>
            <listitem>
              <term>fwl</term>, a positive scalar the  frequency window length
            </listitem>
            <listitem>
              <term>twl</term>, a positive scalar the time window length
            </listitem>
            <listitem>
              <term>method</term>, a character string with possible
              values: <literal>"sp"</literal> (spectrogram),
              <literal>"spwv"</literal> (smoothed pseudo
              Wigner-Ville), <literal>""zam"</literal>
              (Zao-Atlas-Marks time-frequency distribution),
              <literal>"bj"</literal> (sBorn-Jordan time-frequency
              distribution),
            </listitem>
            <listitem>
              <term>draw_type</term>, a character string with possible
              values <literal>"pseudocolor"</literal> (2D display done
              by <link type="scilab" linkend="scilab.help/grayplot"
              >grayplot</link> ), <literal>"3d"</literal> (3D display
              done by <link type="scilab"
              linkend="scilab.help/plot3d1">plot3d1</link>)
            </listitem>
            <listitem>
              <term>cmap</term>, a character string with possible
              values <literal>"jet"</literal> (jetcolormap used) or
              <literal>"gray"</literal> (reverted graycolormap used).
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
       This tool can be used to explore the spectrum and the frequency
       evolution of a signal using various time frequency methods and
       various visualization methods.
    </para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
datafile  = CWApath()+"demos/DATA/apnee_spont";
d=read(datafile,-1,7);
freq_sampling=4;
RR=d(:,1);
Vt=d(:,5);

f1=scf(100001);clf;f1.figure_position=[0 0];f1.figure_size=[524 514];
f1.figure_name="TimeFrequency RR apnea";
TimeFrequencyTool(RR,freq_sampling) 

f2=scf(100002);clf;f2.figure_position=[450 0];f2.figure_size=[524 514];
f2.figure_name="TimeFrequency Vt apnea";
TimeFrequencyTool(Vt,freq_sampling) 
    ]]></programlisting>
  <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../images/TF1.jpg"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../images/TF2.jpg"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member><link type="scilab" linkend="stftb.help/Ctfrbj" >Ctfrbj</link></member>
      <member><link type="scilab"  linkend="stftb.help/Ctfrzam" >Ctfrzam</link></member>
      <member><link type="scilab"  linkend="stftb.help/Ctfrspwv" >Ctfrspwv</link></member>
      <member><link type="scilab" linkend="stftb.help/Ctfrsp" >Ctfrsp</link></member>
      <member><link type="scilab" linkend="scilab.help/grayplot" >grayplot</link></member>
      <member><link type="scilab" linkend="scilab.help/plot3d1" >plot3d1</link></member>

    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
  <refsection>
  <title>Used Functions</title>
  <para>
    Mainly based on the tome frequency toolbox stftb
  </para>
  </refsection>
</refentry>
