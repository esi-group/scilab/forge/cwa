<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Cardiovascular Wawes Analysis toolbox
Copyright (C) 2014 - INRIA - Serge Steer
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="ECGFindPVC">
  <refnamediv>
    <refname>ECGFindPVC</refname>
    <refpurpose>Localize premature ventricular contraction with  compensatory pause.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>kesv = ECGFindPVC(kRp)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>kRp</term>
        <listitem>
          <para>
            A vector, the indexes of th R peaks in the ECG signal.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>kesv</term>
        <listitem>
          <para>
            a vector the indexes of R peaks corresponding to the premature ventricular contraction  with compensatory pause.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      Extra systoles are extra beats, which interrupt the normal
      regular rhythm of the heart. They occur when there is an
      electrical discharge from somewhere in the heart other than the
      sino-atrial node. They are classified as atrial or ventricular
      extra systoles according to their site of origin. The ventricular
      extra systoles are the most common type of arrhythmia that occurs
      after myocardial infarction.  The ventricular extra systoles or
      premature ventricular contractions are characterized by a
      enlarged QRS complex. The premature ventricular contractions
      with a compensatory pause are characterized by a short RR
      interval followed by a long RR interval, the sum of the length
      of these RR intervals being nearly the double of a normal RR.
    </para>
 
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
S=readEcgsFile(CWApath()+"demos/datafiles/VE.ecgs");
viewECG(S(:,1));
kRp=ECGRpeaks(S(:,1));
kPVC=ECGFindPVC(kRp)
plot(kRp(kPVC)'/S.fs,S.sigs(kRp(kPVC)),'+r')
    ]]></programlisting>
    <scilab:image><![CDATA[
S=readEcgsFile(CWApath()+"demos/datafiles/VE.ecgs");
set(gcf(),"axes_size", [610,240])
viewECG(S(:,1));
kRp=ECGRpeaks(S(:,1));
kPVC=ECGFindPVC(kRp)
plot(kRp(kPVC)'/S.fs,S.sigs(kRp(kPVC)),'+r')
    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="ECGRpeaks">ECGRpeaks</link>
      </member>
      <member>
        <link linkend="ECGDetections">ECGDetections</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Claire Médigue, INRIA</member>
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
         <ulink url="http://en.wikipedia.org/wiki/Premature_ventricular_contraction">http://en.wikipedia.org/wiki/Premature_ventricular_contraction</ulink>
       </para>
     </refsection>
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function  added</revdescription>
        </revision>
      </revhistory>
    </refsection>

</refentry>
