<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
 Copyright (C) 2014 - INRIA - Serge Steer
 This file must be used under the terms of the CeCILL.
 This source file is licensed as described in the file COPYING, which
 you should have received as part of this distribution.  The terms
 are also available at
 http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="readEcgsFile">
  <refnamediv>
    <refname>readEcgsFile</refname>
    <refpurpose>Get the ECG data out of an Scilab ECG file.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>ecg = readEcgsFile(filein [,from [,N]])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>filein</term>
        <listitem>
          <para>
            A character string, the path of the Scilab ECG file.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>from</term>
        <listitem>
          <para>
             a scalar with integer value. It can be used to get only a
             sequence of the ECG and specifies the index of the first
             sample of the sequence. The default value is 1.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>N</term>
        <listitem>
          <para>
             a scalar with integer value, the length of the sequence
             to be read. If omitted the sequence goes to the end.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>S</term>
        <listitem>
          <para>
             A <link linkend="sciecg">sciecg</link> data structure.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>fe</term>
        <listitem>
          <para>
            a positive scalar, the sampling frequency in Hz.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
       This function can be used to get the data of a multi channels
       Scilab ECG file.
    </para>
  </refsection>
  <refsection>
    <title>More information</title>
    <tip><para>The <link
    linkend="ecgsFileInfo">ecgsFileInfo</link> may be used to get
    the sampling frequency, number of channels and number of samples of the Scilab ECG
    file.</para></tip>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
S=readEcgsFile(CWApath()+"demos/datafiles/ecg1.ecgs");
viewECG(S(1:5000))
    ]]></programlisting>
    <scilab:image><![CDATA[
S=readEcgsFile(CWApath()+"demos/datafiles/ecg1.ecgs");
viewECG(S(1:5000))
set(gcf(),"axes_size",[610,220])
    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member><link linkend="ecgsFileInfo">ecgsFileInfo</link></member>
      <member>
        <link linkend="extractPartFromEcgsFile">extractPartFromEcgsFile</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
 <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function added</revdescription>
        </revision>
      </revhistory>
    </refsection>
  
</refentry>
