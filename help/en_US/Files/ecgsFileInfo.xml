<?xml version="1.0" encoding="UTF-8"?>
<!--
 This file is part of the Cardiovascular Wawes Analysis toolbox
 Copyright (C) 2014 - INRIA - Serge Steer
 This file must be used under the terms of the CeCILL.
 This source file is licensed as described in the file COPYING, which
 you should have received as part of this distribution.  The terms
 are also available at
 http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="ecgsFileInfo">
  <refnamediv>
    <refname>ecgsFileInfo</refname>
    <refpurpose>Return information about Scilab ECG file.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[fe,Nchannels,Nsamples,t0] = ecgsFileInfo(filein)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>filein</term>
        <listitem>
          <para>
            A character string, the path of the Scilab ECG file.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>fe</term>
        <listitem>
          <para>
             a positive scalar, the sampling frequency in Hz.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Nchannels</term>
        <listitem>
          <para>
            a scalar with integer value, the number of channels of the ECG.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Nsamples</term>
        <listitem>
          <para>
            a scalar with integer value, the number of samples of the ECG.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>t0</term>
        <listitem>
          <para>
            a vector, the date of the first sample [year  month day hour minute second]
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      This function returns information about Scilab ECG file.  
    </para>
  </refsection>
  
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
[fs,NChannels,NSamples,t0]=ecgsFileInfo(CWApath()+"demos/datafiles/LG.ecgs")
    ]]></programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
     <member><link linkend="readEcgsFile">readEcgsFile</link></member>
     <member><link linkend="getValuesFromEcgsFile">getValuesFromEcgsFile</link></member>
 
    </simplelist>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>
  
    <refsection>
       <title>History</title>
      <revhistory>
        <revision>
          <revnumber>0.0</revnumber>
          <revdescription>Function added</revdescription>
        </revision>
      </revhistory>
    </refsection>

</refentry>
