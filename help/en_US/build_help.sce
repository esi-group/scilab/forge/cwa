// This file is released under the 3-clause BSD license. See COPYING-BSD.
function builder_help()
  smd=getscilabmode()
  if and(smd <> ["STD" "NW"]) then
    error(msprintf(gettext("%s: documentation cannot be built in this scilab mode: %s.\n"),...
                   "build_help.sce",smd));
  end

  path=get_absolute_file_path("build_help.sce");
  srcfiles=[path+"date_build"
            ls(path+"*.xml")
            path+"build_help.sce"];
  for d=["Analysis/" "Detections/" "Files/" "Pretreatment/"  "Tests/" "Tools/" "Visualization/"]
     srcfiles=[srcfiles;
            ls(path+d+"*.xml")];
  end
  if newest(srcfiles)==1 then return;end
  xmltojar(path,TOOLBOX_TITLE,"en_US");
  mputl(sci2exp(getdate()),path+"date_build")  
endfunction
builder_help()
clear builder_help;

