/*
* Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
* Copyright (C) INRIA - 2012 - Serge Steer
*
* This file must be used under the terms of the CeCILL.
* This source file is licensed as described in the file COPYING, which
* you should have received as part of this distribution.  The terms
* are also available at
* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
*
*/
#include <time.h>
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "malloc.h"
#include "wfdb.h"
#define map1
#define map2
#define ammap
#define mamap
#define annpos
#include "ecgmap.h"

char *expandPathVariable(char* str);

#if SCI_VERSION_MAJOR < 6
int sci_rdann(char *fname, unsigned long l)
#else
int sci_rdann(char *fname, void* pvApiCtx)
#endif
{
  int k=0,i=0, m=0, n=0;
  SciErr sciErr;
  int *piAddressVar = NULL;
  char *record = NULL;
  char *annotator = NULL;
  char **aux = NULL;
  char *e="";

  int iRows = 5;
  int iCols = 0;
  int nin=nbInputArgument(pvApiCtx);
  int nout= nbOutputArgument(pvApiCtx);

  double *res = NULL;
  double *timebounds=NULL;
  double sps = 0.;
  double t = 0;
  static WFDB_Anninfo ai;
  WFDB_Annotation annot;
  char *expandedPath=NULL;

  CheckInputArgument(pvApiCtx, 2, 3);
  CheckOutputArgument(pvApiCtx, 1, 2);

  /*Record name */
  sciErr = getVarAddressFromPosition(pvApiCtx,1 , &piAddressVar);

  if( getAllocatedSingleString(pvApiCtx, piAddressVar, &record)) goto ERR;
  expandedPath = expandPathVariable(record);
  freeAllocatedSingleString(record);
  if (expandedPath==NULL) {
    Scierror(999, _("%s: No more memory.\n"), fname);
    return 0;
  }
  /* Annotator */
  sciErr = getVarAddressFromPosition(pvApiCtx,2 , &piAddressVar);

  if(getAllocatedSingleString(pvApiCtx, piAddressVar, &annotator)) goto ERR;

  if (nin>=3) { /*time selection*/
    sciErr = getVarAddressFromPosition(pvApiCtx,3 , &piAddressVar);
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVar, &m, &n, &timebounds);
    if(sciErr.iErr)
      {
        printError(&sciErr, 0);
        goto ERR;
      }
    if (m*n != 2 && m*n != 0) {
      Scierror(999,_("%s: Wrong size for input argument #%d: %d or %d expected.\n"),"wfdbReadAnnotations",3,0,2);
      goto ERR;
    }
    if (timebounds[0] >= timebounds[1]) {
      Scierror(999,_("%s: Wrong values for input argument #%d: Elements must be in increasing ""order.\n"),"wfdbReadAnnotations",3);
      goto ERR;
    }
  }
  wfdbquiet();
  if ((sps = sampfreq(expandedPath)) < 0.)
    (void)setsampfreq(sps = WFDB_DEFFREQ);
  ai.name = annotator;
  ai.stat = WFDB_READ;
  if (annopen(expandedPath, &ai, 1) < 0) {
    Scierror(999,_("%s: wfdb  error: %s.\n"),"wfdbReadAnnotations",wfdberror());
    goto ERR;
  }
   /* first pass to get the number of annotations*/
  if (timebounds != NULL) {
    while (getann(0, &annot) == 0) {
      t=annot.time/sps;
      if (t>timebounds[1]) break;
      if (t>=timebounds[0]) iCols++;
    }
  }
  else
    while (getann(0, &annot) == 0) iCols++;
  iannclose(0);

  sciErr = allocMatrixOfDouble(pvApiCtx, nin+1, iRows, iCols, &res);
  if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      goto ERR;
    }
  if (annopen(expandedPath, &ai, 1) < 0) {
    Scierror(999,_("%s: wfdb  error: %s.\n"),"wfdbReadAnnotations",wfdberror());
    goto ERR;
  }
  if (nout==2) {
    if ((aux = malloc(iCols * sizeof(char *))) == NULL) {
      Scierror(999,_("%s: Cannot allocate more memory.\n"),"rdsamp");
      goto ERR;
    }
    for (i=0;i<iCols;i++) aux[i]=e;
  }
  /* first pass to get the number of annotations*/
  k=0;i=0;
  if (timebounds != NULL) {
    while (getann(0, &annot) == 0) {
      t=annot.time/sps;
      if (t>timebounds[1]) break;
      if (t>=timebounds[0]) {
        res[k]=t;
        res[k+1]=annot.anntyp;
        res[k+2]=annot.subtyp;
        res[k+3]=annot.chan;
        res[k+4]=annot.num;
        if (nout==2&&annot.aux != NULL)  aux[i]=annot.aux + 1;
        k=k+5;
        i++;
      }
    }
  }
  else {
    while (getann(0, &annot) == 0) {
      res[k]=annot.time/sps;
      res[k+1]=annot.anntyp;
      res[k+2]=annot.subtyp;
      res[k+3]=annot.chan;
      res[k+4]=annot.num;
      if (nout==2&&annot.aux != NULL)  aux[i]=annot.aux + 1;
      k=k+5;
      
      i++;
    }
  }
  AssignOutputVariable(pvApiCtx, 1) = nin+1;
  if (nout==2) {
    sciErr = createMatrixOfString(pvApiCtx, nin + 2, 1, iCols, aux);
    if(sciErr.iErr)
      {
        printError(&sciErr, 0);
        goto ERR;
      }
    AssignOutputVariable(pvApiCtx, 2) = nin+2;
  }
  FREE(expandedPath);
  expandedPath = NULL;
  free(aux);
  freeAllocatedSingleString(annotator);
  ReturnArguments(pvApiCtx);
  return 0;
 ERR:
  FREE(expandedPath);
  expandedPath = NULL;
  free(aux);
  freeAllocatedSingleString(annotator);
  return 0;
}
