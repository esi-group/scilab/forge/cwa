#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
void fasper(double *x, double *y, unsigned long n, double ofac, double hifac,  double *frq, double* power, unsigned long maxout,int *ierr);


#if SCI_VERSION_MAJOR < 6
int sci_Lomb(char *fname, unsigned long l)
#else
int sci_Lomb(char *fname, void* pvApiCtx)
#endif
{
  int k, m,n,ierr,kfrq,kpower;
  unsigned long Nsamples, ndim, nout, maxout;
  double* x = NULL;
  double* y = NULL;
  double* power = NULL; /*power*/
  double* frq = NULL;/*frequency*/


  double ofac=4, hifac=2,temp;
  SciErr sciErr;
  int *piAddressVar = NULL;
  int nin= nbInputArgument(pvApiCtx);

  CheckInputArgument(pvApiCtx, 2, 5);
  CheckOutputArgument(pvApiCtx, 2, 2);
  
 

  /*x*/
  k=1;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
  sciErr=getMatrixOfDouble(pvApiCtx, piAddressVar, &m, &n, &x);

  /*y*/
  k=2;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
  sciErr=getMatrixOfDouble(pvApiCtx, piAddressVar, &m, &n, &y);

  Nsamples = m*n;

  if (nin>2) {
    /*ofac*/
    k=3;
    sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
    if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
    if (!isScalar(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong size for input argument #%d.\n"), fname, k);
      return 0;
    }
    getScalarDouble(pvApiCtx, piAddressVar, &ofac);
  }
  if (nin>3) {
    /*hifac*/
    k=4;
    sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
    if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
    if (!isScalar(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong size for input argument #%d.\n"), fname, k);
      return 0;
    }
    getScalarDouble(pvApiCtx, piAddressVar, &hifac);
  }
  /* Allocate output variables */
  nout = (unsigned long)(0.5*ofac*hifac*Nsamples);
  maxout=nout;
  if (nin>4) {
    /*maxout*/
    k=5;
    sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
    if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
    if (!isScalar(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong size for input argument #%d.\n"), fname, k);
      return 0;
    }
    getScalarDouble(pvApiCtx, piAddressVar, &temp);
    maxout=(unsigned long)temp;
    if (maxout>=nout) {
      Scierror(999, _( "%s: Wrong value for input argument #%d: Must be < %d.\n"), fname, k,nout);
      return 0;
    }
  }
  /*frq*/
  kfrq=k+1;
  sciErr = allocMatrixOfDouble(pvApiCtx, kfrq, 1, maxout, &frq);
  if(sciErr.iErr) {
    printError(&sciErr, 0);
    return 0;
  }
  /*power*/
  kpower=kfrq+1;
  sciErr = allocMatrixOfDouble(pvApiCtx, kpower, 1, maxout, &power);
  if(sciErr.iErr) {
    printError(&sciErr, 0);
    return 0;
  }


  fasper(x, y, Nsamples, ofac, hifac, frq, power, maxout,&ierr);
  if (ierr!=0) {
    Scierror(999,_("%s: Cannot allocate more memory.\n"),fname);
    return 0;
  }

  AssignOutputVariable(pvApiCtx, 1) = kfrq;
  AssignOutputVariable(pvApiCtx, 2) = kpower;

  ReturnArguments(pvApiCtx);
  return 0;
}
