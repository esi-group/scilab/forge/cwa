#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
void SampEn(double *y, int M, double r, int n, double *p,double *A,double *B,long *iw);

#if SCI_VERSION_MAJOR < 6
int sci_SampEn(char *fname, unsigned long l)
#else
int sci_SampEn(char *fname, void* pvApiCtx)
#endif
{
  int k,kp,kA,kB,kiw, m,n,ierr;
 
  double* y = NULL;
  double* p = NULL;
  double* A = NULL;
  double* B = NULL;
  double *iw = NULL;
  double r;
  double s,st;
  int M;
 
  SciErr sciErr;
  int *piAddressVar = NULL;
  
  int nin= nbInputArgument(pvApiCtx);
  CheckInputArgument(pvApiCtx,1,3);
  CheckOutputArgument(pvApiCtx, 1,3);
  
 

  /*y*/
  k=1;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
  sciErr=getMatrixOfDouble(pvApiCtx, piAddressVar, &m, &n, &y);
  n=m*n;
  /*M*/
  if (nin>=2) {
    double temp;
    k++;
    sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
    if (getScalarDouble(pvApiCtx, piAddressVar, &temp)!=0) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
    M=(int)temp;
  }
  else M=5;
  
  /*r*/
 if (nin>=3) {
    k++;
    sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
    if (getScalarDouble(pvApiCtx, piAddressVar, &r)!=0) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
    
  }
  else r=0.2;


 
  /*p*/
  kp=k+1;
  sciErr = allocMatrixOfDouble(pvApiCtx, kp, M,1, &p);
  if(sciErr.iErr) {
    printError(&sciErr, 0);
    return 0;
  }
  /*A*/
  kA=kp+1;
  sciErr = allocMatrixOfDouble(pvApiCtx, kA, M, 1, &A);
  if(sciErr.iErr) {
    printError(&sciErr, 0);
    return 0;
  }
  /*B*/
  kB=kA+1;
  sciErr = allocMatrixOfDouble(pvApiCtx, kB, M, 1, &B);
  if(sciErr.iErr) {
    printError(&sciErr, 0);
    return 0;
  }
  /*iw*/
  kiw=kB+1;
  sciErr = allocMatrixOfDouble(pvApiCtx, kiw, 1, 2*n, &iw);
  if(sciErr.iErr) {
    printError(&sciErr, 0);
    return 0;
  }

  SampEn(y, M, r,  n, p,A,B,iw);

  AssignOutputVariable(pvApiCtx, 1) = kp;
  AssignOutputVariable(pvApiCtx, 2) = kA;
  AssignOutputVariable(pvApiCtx, 3) = kB;

  ReturnArguments(pvApiCtx);
  return 0;
}
