#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "malloc.h"
#include "sciprint.h"
extern int Levkov(double *src,double *des,int N,double M,int num, double *temp);

#if SCI_VERSION_MAJOR < 6
int sci_Levkov(char *fname, unsigned long l)
#else
int sci_Levkov(char *fname, void* pvApiCtx)
#endif
{
  int k=0;
  long fs, f;
  double t=0.0;
  double w=0.0;
  int m,n,N,Nsamples;
  double Threshold;
  double* src = NULL;
  double* des = NULL;
  double* temp = NULL;
  SciErr sciErr;
  int *piAddressVar = NULL;

  CheckInputArgument(pvApiCtx, 4, 4);
  CheckOutputArgument(pvApiCtx, 1, 1);


  /*src*/
  k=1;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
    }
  sciErr=getMatrixOfDouble(pvApiCtx, piAddressVar, &m, &n, &src);
  Nsamples = m*n;
  /*fs*/
  k=2;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
  }
  if (!isScalar(pvApiCtx, piAddressVar)) {
    Scierror(999, _("%s: Wrong size for input argument #%d.\n"), fname, k);
    return 0;
  }
  getScalarDouble(pvApiCtx, piAddressVar, &w);
  fs = (int)w;

  /*f*/
  k=3;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
  }
  if (!isScalar(pvApiCtx, piAddressVar)) {
    Scierror(999, _("%s: Wrong size for input argument #%d.\n"), fname, k);
    return 0;
  }
  getScalarDouble(pvApiCtx, piAddressVar, &w);
  f = (int)w;
 
  /*Threshold*/
  k=4;
  sciErr = getVarAddressFromPosition(pvApiCtx,k , &piAddressVar);
  if (!isDoubleType(pvApiCtx, piAddressVar)||isVarComplex(pvApiCtx, piAddressVar)) {
      Scierror(999, _("%s: Wrong type for input argument #%d.\n"), fname, k);
      return 0;
  }
  if (!isScalar(pvApiCtx, piAddressVar)) {
    Scierror(999, _("%s: Wrong size for input argument #%d.\n"), fname, k);
    return 0;
  }
  getScalarDouble(pvApiCtx, piAddressVar, &Threshold);

  k=5;
  sciErr = allocMatrixOfDouble(pvApiCtx, k, m, n, &des);
  if(sciErr.iErr) {
    printError(&sciErr, 0);
    return sciErr.iErr;
  }

  N=fs/f;
  temp = (double*)malloc(sizeof(double)*(N));
  Levkov(src,des,N,Threshold,Nsamples,temp);
  free(temp);
  AssignOutputVariable(pvApiCtx, 1) = k;
  ReturnArguments(pvApiCtx);
  return 0;
}
