//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function MultiChannelScrollMenu(k,win)
//Utilitary function for MultiChannelPlot function
  fig=get_figure_handle(win)
  scf(fig)
  select k
  case 1  then //set Scroll length
    C=fig.children;
    C=C(C.type=="Axes");
    ud=fig.user_data;
    count=ud.scroll(1);
    step=ud.scroll(2);
    [ok,l]=uigetlength(_("Give the length of the scrolling window"),step) 
    if ~ok then return,end
    ud.scroll(1:2)=[floor(count*step/l),l];
    set(fig,"user_data",ud);
  case 2  then //Scroll On
    ud=fig.user_data;
    fig.event_handler_enable='off';
    fig.event_handler="MultiChannelPlotHandler";
    fig.event_handler_enable='on';
    xinfo(_("select time window with left and right directionnal arrows, q for full view"))
  case 3  then //Scroll Off
    fig.event_handler_enable='off';
     xinfo("")
  end
endfunction
