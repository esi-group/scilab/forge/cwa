//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Paulo Gonça
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [dTau,F,Q,Tau]=MultilevelAnalysis(Sig,N,varargin)
//function [dTau,F,Q,Tau]=LargeDeviationsSpectrum(Sig,N,varargin)
//computes the Large deviations spectrum of a 1D signal
// Sig: a real vector, the signal
// N a vector with integer values, the dyadic levels requested
// Optional arguments
// The optional argument can be given by a sequence key, value
// the valid keys are:
//   "Q" : the associated value must be a real vector, The vector od the
//         requested "moment" values
//   "eps_flag" : The associated value must be a number with possible values:
//              1: eps computed using log(log(N)) factor 
//              2: eps computed using  log(N) factor 
//              3: eps computed using  factor 1 
//              where eps is the box length.
//   "t" : allows to specify time instants corresponding to the Sig values.
//         the associated value must be a real vector with identical size
//         as Sig.
//         if t is given the dyadic intervals are computed with respect
//         to the t values.
//   "xsi": a real valued, non identically null, non negative function
//           which returns a value for a given dyadic interval (number of
//           points in the box). 
//          The default value is the oscillation function: function
//          r=xsi(x),r=max(x)-min(x);endfunction 
//
// Tau : Tau(k) is  the correlation exponent or mass exponent of order Q(k)
// dTau : dTau(k) is the derivative of Tau(k) with respect to q.
//         It is also the Lipschitz–Holder exponent alpha(q) for Q(k)
// F    : F(:,l) is the multifractal spectrum f(alpha) for the lth level
//        of dyadic intervals
//See "On the Estimation of the Large Deviation Spectrum" J.Barral, P. Gonçalves, Springer Verlag


   //default values
   function r=osc(x),r=max(x)-min(x);endfunction
   Q = logspace(log10(0.01),log10(15),100) ; Q = [-Q($:-1:1) 0 Q] ;
   t=[];
   eps_flag=1
   //optional parameters
   for k=1:2:size(varargin)
     select varargin(k)
     case "Q" then 
       Q=varargin(k+1)
     case "eps_flag" then
       eps_flag=varargin(k+1)
     case "t" then
       t=varargin(k+1)
       if size(t,'*')<>size(Sig,'*') then
         error(msprintf(_("%s: Wrong size for arguments #%d and #%d: Incompatible length.\n"),"MultilevelAnalysis",1,3+k))
       end
     case "xsi" then
       osc=varargin(k+1);
       if type(osc)<>13 then
         error(msprintf(_("%s: Wrong type for input argument #%d: A f"+...
                          " unction expected.\n"),"MultilevelAnalysis",3+k))
       end
     else
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set  {%s}.\n"),...
                      "MultilevelAnalysis",2+k,"""Q"",""eps_flag"",""t"",""xsi"""))
     end
   end
   select eps_flag
   case 1 then
     function Eps=Epsfun(d2Tau,n),Eps = sqrt(-2*min(d2Tau,0)*log(log(n))/(n*log(2)));endfunction     
   case 2 then
     function Eps=Epsfun(d2Tau,n),Eps = sqrt(-2*min(d2Tau,0)*log(n)/(n*log(2)));endfunction     
   case 3 then
     function Eps=Epsfun(d2Tau,n),Eps = sqrt(-2*min(d2Tau,0)/(n*log(2)));endfunction     
   end
   
   nQ=size(Q,'*');
   ns=size(Sig,'*')
   Sig=Sig-min(Sig);
   Sig=Sig/max(Sig);
   nl=size(N,'*')
   ieee_save=ieee();ieee(2)
   
  F = zeros(nQ,nl);
  dTau = zeros(nQ,nl);
  Tau = zeros(nQ,nl);
  for in=1:nl
    n = N(in);
    m = 2^n;
    Osc=zeros(1,m);
    if t<>[] then
      //split the time range into a set G_n of 2^n intervals I_n
      In=linspace(min(t),max(t),m+1);
      [ind, occ, info] = dsearch(t,In);
      ind=[0 cumsum(occ)]+1;//the intervals are indexed by I=ind(i):ind(i+1)-1
      //Compute the oscillation for each dyadic intervals
      for i=1:m, Osc(i) = osc(Sig(ind(i):ind(i+1)-1));end
    else
      Sig = matrix(Sig(1:2^floor(log2(ns))),-1,m);
      //Compute the oscillation for each dyadic intervals
      for i=1:m, Osc(i) = osc(Sig(:,i));end
    end
    if ~isreal(Osc) then
      ieee(ieee_save)
      error(msprintf(_("%s: xsi function returned a complex value for level %d\n."),"MultilevelAnalysis",n))
    end
    if or(Osc<0) then
      ieee(ieee_save)
      error(msprintf(_("%s: xsi function returned a negative value for level %d\n."),"MultilevelAnalysis",n))
    end
     if and(Osc==0) then
      warning(msprintf(_("%s: xsi function returned 0 for all intervals at level %d\n."),"MultilevelAnalysis",n))
    end
    
    //compute the roughness (grain) exponent on each interval
    Osc = Osc(Osc<>0);
    if Osc==[] then
      dTau(:,in) =%nan;
      F(:,in) = %nan;
    else
      AlphaI = -log2(Osc)/n;
      
      for iq = 1:length(Q), 
        q = Q(iq);
        Tau(iq,in) = -log2(sum(Osc.^q))/n;
        //compute the first derivative of tau with respect to q
        dTau(iq,in) = -sum((Osc.^q).*log2(Osc))/(n * sum(Osc.^q));
        //compute the second derivative of tau with respect to q
        d2Tau = (-sum((Osc.^q).*(log(Osc).^2))/sum(Osc.^q)...
                 +(sum((Osc.^q).*log(Osc))/sum(Osc.^q))^2)/(n*log(2));
        Eps = Epsfun(d2Tau,n);
        F(iq,in) = size(find(AlphaI>dTau(iq,in)-Eps&AlphaI<dTau(iq,in)+Eps),'*');
      end
    end
    F(:,in) = log2(F(:,in))./n;
  end
   ieee(ieee_save)
endfunction
