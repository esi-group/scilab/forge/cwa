//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [S,f]=readTMS(f,sel,NoS)
// f : file path or data structure returned by readTMSHeader or readTMS
// sel: vector of the index of the requested channels or [] for all
// NoS: number of sample to be read. Default is all.  
// derived from: http://www.tmsi.com/downloads/tms_read.m
  if type(f)==10 then
    u=mopen(f,'rb');
    [HDR,f]=readTMSHeader(u)
  else
    u=f.fid;
  end
  SPR=f.SPR;   //number of samples per record
  Nsamples=f.Nsamples; //nombre total d'échantillons multivoies contenus dans le fichier
  GDFTYP=f.GDFTYP;
  NS=f.Nchannels
  NR=f.NR;//the number of sample remaining in the current block
  NB=f.NB;//the number of remaining full blocks
  if argn(2)<2|sel==[] then sel=1:NS;end
  if argn(2)<3 then  NoS = Nsamples;end
  
  RemainingSamples=NR+NB*SPR
  NoS=min(NoS,RemainingSamples),
  S(1:NoS,1:length(sel))=%nan

  //read samples remaining in the current block (NR a definir)
  n=0;
  //mprintf("NR=%d\n",NR)
  if NR>0 then
    exec(readDataInBlock,-1);
    n=n+ns
  end
  
  if NB==0 then  mclose(u);return;end 
    
  nfullblock=min(NB,floor((NoS-n)/SPR));//Le nombre de blocs à lire en totalité
  //Jump to the next block
  [PI,dt]=getblockheader();
  for bc=1:nfullblock
    //Read a block of data
    NR=SPR;
    exec(readDataInBlock,-1);
    n=n+ns
    [PI,dt]=getblockheader();
  end;
 
  if n<NoS then
     //Read first samples of the last block
     NR=NoS-n;
     exec(readDataInBlock,-1);
  else
    mseek(-86,u,"cur")
  end
  f.Nsamples=Nsamples-NoS; //nombre total d'échantillons multivoies restant à lire dans le fichier
  f.NR=SPR-NR;//the number of sample remaining in the current block
  f.NB=NB-nfullblock
  if NoS==RemainingSamples then 
    f.NB=0
    f.NR=0
    mclose(u);
  end
  r=(f.UnitHigh - f.UnitLow)./(f.ADCHigh - f.ADCLow);
  
  for k=1:size(sel,'*')
    k1=sel(k);
    S(:,k)=(S(:,k)-f.ADCLow(k1))*r(k1)+f.UnitLow(k1)
  end
  S=sciecg(HDR.SampleRate,S/1000);
endfunction


function s=readDataInBlock()
//Read samples in the current block
//this function is intended to exec'ed to avoid copies
  ns=NR
  if and(GDFTYP==GDFTYP(1)) then
    //all channels are coded in a same way
    s =matrix(mget(NS*NR,gdfdatatype(GDFTYP(1)),u),NS,-1);
    if meof(u) then ns=size(s,2),end
  else
    // channels may be coded differently one fron the other
    s(1:NS,1:NR) = %nan;
    for k1 = 1:NR,
      for k2 = 1:NS,
        s(k2,k1) = mget(1,gdfdatatype(GDFTYP(k2)),u);
      end;
      if meof(u) then
        ns=k1-1
        break
      end
    end;
  end;
  S(n+1:n+ns,:) = s(sel, 1:ns)';
endfunction
function [PI,dt]=getblockheader()
  PI=mget(1,'i',u) //period index
  mget(4,'uc',u);//reserved
  dt=mget(7,'s',u)// time
  mget(64,'uc',u);//reserved
endfunction
