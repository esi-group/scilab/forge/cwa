function outliers=medianFilter(s,medLimit)
  sM=median(s);
  med=median(abs(s-sM));
  D=abs(s-sM)./(1.483*med);
  outliers=D>medLimit;                
endfunction       

