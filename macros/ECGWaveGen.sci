//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [s,ds]=ECGWaveGen(T,G)
//Generates a synthetic ECG wave decribed by a sum of gaussian.
  //G=[pos amp wl,f]
//The ECG is assumed periodic with 1Hz  beat frequency 
  if argn(2)==0 then //demo
      //        P     Q      R      S       T    
      pos=  [ 0.35;   0.47;  0.5;   0.53;  0.8];
      amp=  [ 0.051; -0.1;   0.8;  -0.18;  0.2] ;
      wl=   [ 0.1;    0.03;  0.08;  0.03;  0.3];
      wr=   [ 0.1;    0.03;  0.08;  0.03;  0.18];
      bl=   [ 0;      0;     0;     0;     0];
      br=   [ 0;      0;     -0.1;  0;     0.1];
      t=linspace(0,1,1000);
      s=ECGWaveGen(t,[pos,amp,wl,wr bl br]);
      clf();plot(t,s);
      ds=[]
      return
  end
  with_grad=argn(1)==2
  [ng,np]=size(G)
  select np
  case 3
    pos=G(:,1);
    amp=G(:,2);
    wl=G(:,3);
    wr=wl
    bl=zeros(ng,1);
    br=bl;
  case 4
    pos=G(:,1);
    amp=G(:,2);
    wl=G(:,3);
    wr=G(:,4);
    bl=zeros(ng,1);
    br=bl;
  case 6 then
    pos=G(:,1);
    amp=G(:,2);
    wl=G(:,3);
    wr=G(:,4);
    bl=G(:,5);
    br=G(:,6);
  end
  T=matrix(T,1,-1)
  nT=size(T,'*')
  if with_grad then ds=zeros(ng,6,nT);end
  s=zeros(T);
  c=4*%pi^2;
 
  for k=1:ng
    [m,i]=min(abs(T-pos(k)));
    i=min(i,nT-1);
    Tlk=T(1:i)-pos(k);Trk=T(i+1:$)-pos(k);
    elk=exp(-(0.5*c/wl(k)^2)*(Tlk.^2));
    erk=exp(-(0.5*c/wr(k)^2)*(Trk.^2));
    
    sk=[bl(k)+(amp(k)-bl(k))*elk, br(k)+(amp(k)-br(k))*erk];
    s=s+sk;

    if with_grad then 
      ds(k,1,:) = [(c/wl(k)^2)*(amp(k)-bl(k))*(Tlk.*elk), (c/wr(k)^2)*(amp(k)-br(k))*(Trk.*erk)];//pos
      ds(k,2,:) = [elk,                                    erk                              ];//amp
      ds(k,3,:) = [(c/wl(k)^3)*(amp(k)-bl(k))*(Tlk.^2 .*elk), zeros(Trk)];//wl
      ds(k,4,:) = [zeros(Tlk),  (c/wr(k)^3)*(amp(k)-br(k))*(Trk.^2 .*erk)];//wr
      ds(k,5,:) = [1-elk,zeros(Trk)];//bl
      ds(k,6,:) = [zeros(Tlk),1-erk];//br
    end
  end
 
endfunction
