//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [sigma,mu,a,b,e]=fit_gaussian(x,y,sigma,mu)
//Looks for a 1D gaussian which minimize the sum of square of the errors
//between the model and experimental discrete data  given by the array x
//and y
//the gaussian is parametrized as follow.
// y:=b+a*exp(-(x-mu)^2/(2*sigma^2))
  x=x(:);y=y(:);
  if argn(2)<4 then
    [m,k]=max(abs(y));
    mu=x(k);
    a=y(k);
  end
  if argn(2)<3 then
    sigma=0.5;
  end
  b=0;
  [fopt,popt,gopt]=optim(list(optimfun,x(:),y(:)),[sigma;mu;a;b]);
  sigma=popt(1);
  mu=popt(2);
  a=popt(3);
  b=popt(4);
  e=sqrt(fopt);
endfunction


function [f,g,ind]=optimfun(p,ind,x,y)
  with_grad=or(ind==[3 4]);
  if with_grad then
    [v,dvdsigma,dvdmu,dvda]=gaussian(p(1),p(2),p(3),p(4),x);
    e=y-v;
    f=e'*e;
    g=-2*[e'*dvdsigma;e'*dvdmu;e'*dvda;sum(e)];
  else
    e=y-gaussian(p(1),p(2),p(3),p(4),x);
    f=e'*e;
  end
endfunction
