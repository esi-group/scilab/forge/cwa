//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function curves=segmentDensityMap(dx,dy,xy,r)
//dx:    discretization of the x axis 
//dy:    discretization of the y axis 
//xy:    table such as occ(i,j) contains the number of points in the
//       "pixel" [dx(i) dx(i+1)) x [dy(j) dy(j+1)) (see dsearch2D)
//r:     a positive scalar, the relative density level used to fix the
//       delimitations of image part
//curves:a list, the elements of the list are 2 x n arrays that gives the
//       coordinates of the delimiting curves  
  
  curves=list();
  dxy=max(xy)-min(xy)
  z=min(xy)+r*dxy;
  [xc,yc]=contour2di(dx,dy,xy,z*[1 1.1]);
  k=1;n=yc(k);
  while k+yc(k)<size(xc,'*')
  n=yc(k);
  if xc(k)==z then
    curves($+1)=[xc(k+(1:n));yc(k+(1:n))];
  end
  k=k+n+1;
end
endfunction

