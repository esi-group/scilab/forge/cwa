//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function s=ECGCutPLI(s,f,df)
  //This function remoces power-line interference (PLI) from the ECG using
//annulation of frequency components around f
//  ECGcutPLI(S,fe,f)
//  f : PLI frequency  
  if typeof(s)<>"sciecg" then
    error(msprintf(_("%s: Wrong type for argument %d: sciecg data structure expected.\n"),...
                                      "ECGCutPLI",1))
  end
  fe=s.fs
  s=s.sigs
  s=fft(s,-1,1);
  N=size(s,'*');
  if argn(2)<4 then df=0.2;end
  n=floor(fe/(2*f));
  for k=1:floor(fe/(2*f))
    low=(k*f-df)*N/fe
    high=(k*f+df)*N/fe
    s(low:high,:)=0;
    s(N-high+2:N-low+2,:)=0;
  end
  s=real(fft(s,1,1));
  s=sciecg(fe,s)
endfunction
