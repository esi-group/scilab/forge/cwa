//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function t=wfdbAnnot2String(v)
  //See tstring anc cstring definition in wfdb lib/annot.c 
// 0  0         Nothing  
// 1  N		Normal beat 
// 2  L		Left bundle branch block beat
// 3  R		Right bundle branch block beat
// 4  a		Aberrated atrial premature beat
// 5  V		Premature ventricular contraction
// 6  F		Fusion of ventricular and normal beat
// 7  J		Nodal (junctional) escape beat
// 8  A         Atrial premature beat
// 9  S         Supraventricular premature or ectopic beat
// 10 E 	Ventricular escape beat
// 11 j 	Nodal (junctional) escape beat
// 12 /		Paced beat
// 13 Q		Unclassifiable beat
// 14 ~		Change in signal quality
// 15 U		Undefined
// 16 |		Isolated QRS-like artifact 
// 17 U		Undefined
// 18 s		ST segment change
// 19 T		T-wave change 
// 20 *		Systole  
// 21 D		Diastole
// 22 \		comment annotation
// 23 =		Measurement annotation
// 24 p		Peak of P-wave
// 25 B		Bundle branch block beat (unspecified)
// 26 ^		(Non-captured) pacemaker artifact  
// 27 t		Peak of T-wave
// 28 +		Rhythm change
// 29 u		Peak of U-wave
// 30 ?		Beat not classified during learning
// 31 !		Ventricular flutter wave
// 32 [		Start of ventricular flutter/fibrillation
// 33 ]		End of ventricular flutter/fibrillation
// 34 e 	Atrial escape beat
// 35 n 	Supraventricular escape beat
// 36 @		Link to external data (aux contains URL)   
// 37 x		Non-conducted P-wave (blocked APC)
// 38 f		Fusion of paced and normal beat
// 39 (		Waveform onset
// 40 )		Waveform end
// 41 r		R-on-T premature ventricular contraction
// 42 U		Undefined
// 43 U		Undefined
// 44 U		Undefined
// 45 U		Undefined
// 46 U		Undefined
// 47 U		Undefined
// 48 U		Undefined
// 49 U		Undefined
  und="U";
  typecode = [und;"N";"L";"R";"a";
              "V";"F";"J";"A";"S";
              "E";"j";"/";"Q";"~";
              und;"|";und;"s";"T";
              "*";"D";"\";"="; "p";
              "B";"^";"t";"+";"u";
              "?";"!";"[";"]";"e";
              "n";"@";"x";"f";"(";
              ")";"r";und;und;und;
              und;und;und;und;und];
  t=matrix(typecode(v+1),size(v))
endfunction
