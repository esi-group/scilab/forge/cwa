//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [loc,k_last_rpeak]=ECGDetections(S,varargin)
//S   : vector of ECG  pretreated signal
//fe  : sampling period in Hz  
//varargin : option list, a sequence key, value.
//          allowed keys:
//          TpTe_min : min duration between Tp and Te in % of the interpeak interval, used to skip
//                      bifid T wave, default value = 8%
//          PpRp_min : min distance between Pp and Rp in % of the
//                      interpeak interval, used for localization of the P wave peak.
//                      default value = 7%
//          nf       : FIR sgolay filter length, default value=0.025*fe should
//                      be approximately the width in samples at half height
//                      of the P wave
//loc : an 9 rows array 
//       1: kRp the R wave peak locations
//       2: kRe the R wave end locations
//       3: kTo the T wave  onset locations
//       4: kTp the T wave peak locations
//       5: kTe the T wave end locations
//       6: kPo the P wave onset locations
//       7: kPp the P wave peak locations
//       8: kPe the P wave end locations
//       9: kQo the Q wave onset locations
//The missing detections are indicated by a negative value of the
//associated index.
//For each column we have kRp<kRe<kTo<kTe<kTp<kPo<kPp<kPe<kQo assuming
//all locations have been detected for the corresponding beat.
function e=crit(p,m);e=w1.*(p(1)*t1+p(2)-s1);endfunction
  if typeof(S)=="sciecg" then
    fs=S.fs
    S=S.sigs
    if size(S,2)<>1 then
     error(msprintf(_("%s: Wrong size for argument %d: a single channel ecg expected.\n"),...
                                      "ECGDetections",1))
    end
    kopt=1
  elseif type(S)==1&isreal(S) then
    if and(size(S)<>1) then
      error(msprintf(_("%s: Wrong size for argument %d: a single channel ecg expected.\n"),...
                                      "ECGDetections",1))
    end
    S=S(:);
    fs=varargin(1);
    if type(fs)<>1|~isreal(fs)|size(fs,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),...
                     "ECGDetections",2))
    end
    if fs<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),...
                     "ECGDetections",2))
    end
    kopt=2
  else    
    error(msprintf(_("%s: Wrong type for argument %d: a real array or an sciecg data structure expected.\n"),...
                                      "ECGDetections",1))
  end
 
  //handling options
  TpTe_min=8;//min distance between Tp and Te in % of RR
  PpRp_min=15;//min distance between Pp and Rp in % of RR
  nf=2*int(fs/40)+1;//FIR  sgolay filter  length
  
  for k=kopt:2:size(varargin)
    key=varargin(k);
    v=varargin(k+1);
    if type(key)<>10&size(key,"*")<>1 then
       error(msprintf(_("Wrong type for input argument #%d: String expected.\n"),"ECGDetections",2+k))
    end
    if type(v)<>1|~isreal(v)|size(v,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),...
                     "ECGDetections",3+k))
    end
    if v<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),...
                   "ECGDetections",3+k))
    end
    execstr(key+"=v;")
  end
  TpTe_min=TpTe_min/100; //min distance between Tp and Te in fraction of RR
  PpRp_min=PpRp_min/100; //min distance between Pp and Rp in fraction of RR
  //Compute stavisky Golay differentiation matrix
  nf=2*int(fs/40)+1;//FIR  sgolay filter  length
  [B,C]=sgolay(3,nf);

  S = filtbyfft("bp",S-mean(S),fs, [0.5 250]);
  //S=sgolayfilt(S,B)    
  //Fd=wfir("lp",fs/20,[20,0]/fs,"hn",[]); 
   //S=convol(S,Fd)'
   
  //Normalize S, to fix meaning of curvature
  S=S*fs/(max(S)-min(S));

  //Compute some geometric properties
  yfd=sgolaydiff(S,1,C);//derivative
  yfc=sgolaydiff(S,2,C)./((1+yfd.^2).^1.5);//curvature
  clear B C;
  //R peaks localization
  kRp = ECGRpeaks(S,fs);
  Tend = ECGTwaveEnds(S,fs,kRp);
  //localize min an max of derivatives and curvatures
  kmax=find(yfd(1:$-1)>0 & yfd(2:$)<0);//location of local max
  kmin=find(yfd(1:$-1)<0 & yfd(2:$)>0);//location of local min
//  kmaxc=find(yfc(2:$)>0 & yfc(1:$-1)<0)+2;//location of inflection points
  kcmax=find(yfc(2:$-1)>yfc(1:$-2) & yfc(2:$-1)>yfc(3:$));//location of max curvature
  kcmin=find(yfc(2:$-1)<yfc(1:$-2) & yfc(2:$-1)<yfc(3:$));//location of max curvature

  kRp=matrix(kRp,1,-1);
  Te=matrix(Tend,1,-1);
  kTo=[];
  kTp=[];
  kTe=[];
  kPp=[];
  kQo=[];
  kRe=[];
  kPo=[];
  kPn=[];// notch location if any
  kPe=[]
  ks=1
  if size(kRp,'*')<2 then loc=[];return;end
  //Loop on the R-R intervals
  for k=1:size(kRp,'*')-1
    klow=kRp(k);khigh=kRp(k+1);//boundary index of the samples within current R-R interval
    RR=khigh-klow+1; //number of samples in the current R-R interval

    //----------- Re detection
    ind=find(kcmax>klow&kcmax<khigh,1);
     if ind==[] then
       kRe=[kRe,-1];
     else
       kRe=[kRe,kcmax(ind)];
     end
    //----------- T wave 
    //kTp (peak)
    //look at the min after Rp
    klim=klow+RR/4;
//if klow>2000 then pause,end
    [m,klim]=min(S(klow:klow+RR/4));klim=klow+klim-1;
    
    kkmax=kmax(find(kmax>klim&kmax<Te(k)));
    kkmin=kmin(find(kmin>klim&kmin<Te(k)));//for inverted T waves
    if kkmax==[]& kkmin==[] then 
      //aucun pic T detecté
      kTp=[kTp -1];
      kTo=[kTo,-1];
      kTe=[kTe,-1];
    else
      //kTp
      if %f
      if kkmax==[] then
        s_max=-%inf
      else
        [s_max,ks_max]=max(S(kkmax));//plus grands max
      end
      if kkmin==[] then
        s_min=-%inf
      else
        [s_min,ks_min]=max(-S(kkmin));//plus grands min
      end
      if s_max>s_min then
        kTp=[kTp kkmax(ks_max)];
      else
        kTp=[kTp kkmin(ks_min)];
      end
      else
        if  kkmax==[] then
          [s_min,ks_min]=max(-S(kkmin))
          kTp=[kTp kkmin(ks_min)];
        else
          [s_max,ks_max]=max(S(kkmax));//plus grands max
          kTp=[kTp kkmax(ks_max)];
          end
      end
      
      //kTo (premier min avant Tp)
      //km=find(kTp($)>kRp,1)
      ind=find(kmin<kTp($)&kmin>kRe($));
      if ind==[] then
        kTo=[kTo,-1];
      else
        kTo=[kTo,kmin(ind($))];
      end
      //kTe
      kd=kTp($)+TpTe_min*(kRp(k+1)-kRp(k)); //debut de la recherche de tend 20ms apres le Tp
      ind=find(yfd(kd:khigh-1)<0&yfd(kd+1:khigh)>=0,1);
      if ind==[] then
        kTe=[kTe,-1];
      else
        kTe=[kTe,kd+ind+1];
      end
      kTe($)=Tend(k);//for Qinqua solution
    end
    //----------- P wave 
    //kPp (peak)
    kkkmax=kmax(find(kmax>kTe($)&kmax<khigh-RR*PpRp_min));
    kkkmin=kmin(find(kmin>kTe($)&kmin<khigh-RR*PpRp_min));
    if kkkmax==[]&kkkmin==[] then
       kPp=[kPp -1];
    else
      if kkkmax==[] then
        s_max=-%inf;
      else
        [s_max,ks_max]=max(-yfc(kkkmax));
      end
       if kkkmin==[] then
        s_min=-%inf;
      else
        [s_min,ks_min]=max(yfc(kkkmin));
      end
      if s_max>s_min then
        kPp=[kPp kkkmax(ks_max)];
      else
        kPp=[kPp kkkmin(ks_min)];
      end
    end
    
    //kPo (onset) kPn(notch) kPe (end)
    if kPp($)<0 then //no P wave
       kPo=[kPo,-1];
       kPn=[kPn,-1];
       kPe=[kPe,-1];
    else
      //kPo
      //insure kPo being greater than P wave end
      klim=max([Te(k),kPp($)-RR/10]);
      //      ind=find(kcmax<kPp($)&kcmax>klim&yfc(kcmax)>notch_threshold)
      ind=find(kcmax<kPp($)&kcmax>klim)
      if ind==[] then 
        kPo=[kPo,-1];
        kPn=[kPn,-1];
      else
        [w,ki]=max(yfc(kcmax(ind)))
        kPo=[kPo,kcmax(ind(ki))];
        ind=find(kcmax>kPo($)&kcmax<kPp($),1);
        if ind==[] then
          kPn=[kPn,-1];
        else
          kPn=[kPn,kcmax(ind)];
        end
      end
   
      //kPe
      //ind=find(kcmax>kPp($)&kcmax<khigh&yfc(kcmax)>notch_threshold,1)
      if s_max>s_min then
        ind=kcmax(find(kcmax>kPp($)&kcmax<khigh,1));
      else
        ind=kcmin(find(kcmin>kPp($)&kcmin<khigh,1));
      end

      if ind==[] then 
        kPe=[kPe,-1];
      else
        kPe=[kPe,ind];
      end
      // try to improve Te and Po detection using baseline identification
      if kTe($)<>-1& kPo($)<>-1 then
        //linear regression
        t1=(kTe($):kPo($))';
        if t1<>[] then
        s1=S(t1);
        w1=window('kr',size(t1,'*'),2)';
        c=lsqrsolve([0,0],crit,size(t1,'*'));
        t1=[kRp(k);t1;kRp(k+1)];
//        plot(t1,c(1)*t1+c(2),'r')
//        plot(kTp($),S(kTp($)),'*r',kTe($),S(kTe($)),'+r',kPo($),S(kPo($)),'+g')
        end
      end
    end

    //----------- Qo detection
    ind=find(kcmax>klow+1&kcmax<khigh-1);
    if ind==[] then
       kQo=[kQo,-1];
    else
      kQo=[kQo,kcmax(ind($))];
    end
  end
  loc=[kRp(1:$-1);kRe;kTo;kTp;kTe;kPo;kPp;kPe;kQo];
  k_last_rpeak=kRp($); //used for batch processing
endfunction


