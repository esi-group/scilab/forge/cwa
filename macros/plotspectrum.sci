// This file is part of the ECG toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function plotspectrum(s,fe,opt)
  if argn(2)<3 then opt='b';end
  N=round(size(s,'*')/2);
  f=linspace(0,fe/2,N);
  plot(f,10*log10(s(1:N)),opt)
endfunction
