//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function err=ECGTwaveEnds_vs_annot(name,sigsel,annot,with_plot,debug_flag)
//compare the locations of the T wave ends  returned by ECGTwaveEnds by
//those given in qtdb database cardiologist annotations
  if argn(2)<5 then debug_flag=%f;end
  if argn(2)<4 then with_plot=%f; debug_flag=%f;end
  if argn(2)<3 then annot="q1c";with_plot=%f; debug_flag=%f;end
  margin=4.8;//=1200 echantillons
  ann=wfdbReadAnnotations(name,annot);
  //the annotated T wave ends locations (index)
  kTe_ref=find(ann(2,:)==40&ann(5,:)==2);
  //the annotated  R peaks locations (index)
  kRp_ref=find(ann(2,:)==1);

  //select the part of the signal which contains the T end annotations
  tmin=ann(1,kTe_ref(1))-margin;
  tmax=ann(1,kTe_ref($))+margin;
  [Sigs,fe,n]=wfdbReadSamples(name,%f,[tmin,tmax]);
  Sigs=sciecg(fe,Sigs');
  Sigs=ECGDetrend(Sigs,4);
  Sigs=ECGSubstractPLI(Sigs,50,0.09);
  if sigsel<>[] then
    S=Sigs(:,sigsel)
  else
    S=SynthesisECG(Sigs);
  end
  //shift annotation dates to fit with the select part of the signal
  ann(1,:)=ann(1,:)-tmin+1/fe
  // signal pretreament
 
  //The annotated T wave ends in seconds
  Te_ann=ann(1,kTe_ref);
  Rp_ann=ann(1,kRp_ref);

  // Compute index of R peak location 
  kRp=ECGRpeaks(S);
  Rp=kRp/fe; //R peak location in seconds
  
  
  //search beats that contains an annotated T wave ends
  if Rp($)<Te_ann($) then Rp=[Rp Rp($)+(Rp($)-Rp($-1))];end
  if Rp(1)>Te_ann(1) then Rp=[Rp(1)-(Rp(2)-Rp(1)) Rp];end
  kBeat=zeros(Te_ann);
  for k=1:size(Te_ann,'*')
    kBeat(k)=find(Rp(1:$-1)<Te_ann(k)&Te_ann(k)<Rp(2:$));
  end
  Te_ann=Te_ann(kBeat>0)
  kBeat=kBeat(kBeat>0)
  // Compute T wave ends location in seconds
  if debug_flag then
    debug_data=struct("name",name,"sigsel",sigsel,"Sigs",Sigs,"Te_ann",Te_ann);
    Te=ECGTwaveEnds(S, kRp,"debug",debug_data)'/fe; 
  else
    Te=ECGTwaveEnds(S, kRp)'/fe; 
  end
  //Extract the detected T wave ends located in the selected beats
  keep=zeros(Te_ann);
  for k=1:size(kBeat,'*')
    keep(k)=find(Rp(kBeat(k))<Te&Te<Rp(kBeat(k)+1),1);
  end
  keep=keep(keep>0);
  Te=Te(keep);
  err=1000*(Te-Te_ann); //vector of errors in ms
  if with_plot then
    mn=min(Sigs.sigs);mx=max(Sigs.sigs);
    t=(0:(size(Sigs,1)-1))/fe;
    clf;plot(t,Sigs.sigs),
    xpoly([Te_ann;Te_ann;%nan(ones(Te_ann))],[mn;mx;%nan]*ones(Te_ann));
    e=gce();e.foreground=3;
    xpoly([Te;Te;%nan(ones(Te))],[mn;mx;%nan]*ones(Te))
    e=gce();e.foreground=5;
    legend(["S1";"S2";annot;"ECGTwaveEnds"]);
  end
endfunction
