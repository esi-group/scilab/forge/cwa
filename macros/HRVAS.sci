function output=HRVAS(IBI,settings,with_GUI)
 
  select argn(2)
  case 1 then
    settings=[];
    with_GUI=%t
  case 2 then
    if type(settings)==4 then
      with_GUI=settings
      if ~with_GUI then 
        error("settings argument is mandatory in nogui mode")
      end
      settings=[]
    else
      with_GUI=%t
    end
  case 3 then
  else
    settings=[]
    with_GUI=%t
    IBI=[]
  end
  
  if IBI==[] then
    if ~with_GUI then
      error("IBI argument is mandatory in nogui mode")
    end
  else
    select type(IBI)
    case 10 then //path of an ibi bile
      IBI=loadIBI(IBI)
    case 1 then //ibi array
      IBI=checkIBI(IBI)
    else
      error("Invalid input")
    end
  end
  if with_GUI then
    driver("Rec")
  
    [MainFigure,h]=HRVAS_GUI();
    
    settings=[];//analysis options/settings
    nIBI=[]; //non-detrended ibi
    dIBI=[]; //detrended ibi data
    HRV=[]; //hrv data
    trend=[];
    art=[];
    ////  Initialization Tasks
    
    settings = loadSettings(SCIHOME+filesep()+'HRVAS_settings'); //load parameters and previous options
    setSettings(settings) //set controls to previous options
    ud=tlist(["HRVAS","HRV", "h",  "nIBI", "dIBI", "IBI", "trend", "art","settings"],...
             HRV,   h  , nIBI,   dIBI,    IBI,   trend, art,  settings);
    set(MainFigure,"user_data",ud);
    MainFigure.visible="on";
    if IBI<>[] then  h.menuRun.enable="on";end
    output=MainFigure;
  else
    [output,dIBI,nIBI,trend,art]=getHRV([],IBI,settings)
    output.pre.dIBI=dIBI
    output.pre.nIBI=nIBI
    output.pre.trend=trend
    output.pre.art=art
  end

endfunction
