//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function TimeFrequencyTool(signal,freq_sampling,options)
  fs=freq_sampling;
  N=size(signal,'*')
  fig_ud=struct();
  fig_ud.signal=signal;
  fig_ud.fs=fs;
  fig_ud.freq_step=0.01
  fig_ud.fwl=max(5,round(N/40));
  fig_ud.twl=max(5,round(N/10));
  fig_ud.method="sp"
  fig_ud.draw_type="PseudoColor"
  fig_ud.cmap="jet"
  if argn(2)>=3 then
    F=fieldnames(options)
    for k=1:size(F,'*')
      if and(F(k)<>["freq_step","fwl","twl","method","draw_type","cmap"]) then
        error(msprint(_("%s: Wrong value for input argument #%d: Invalid field ""%s"".\n"),"TimeFrequencyTool",3,F(k)))
      end
      fig_ud(F(k))=options(F(k))
    end
    if and(fig_ud.method<>["sp","spwv","zam","bj"]) then
      error(msprint(_("%s: Wrong value for input argument #%d: Invalid ""%s"" field value.\n"),...
                    "TimeFrequencyTool",3,"method"))
    end
  end
  
  clf();fig=gcf()
  idraw=fig.immediate_drawing;
  fig.immediate_drawing="off";
 
  ax=gca();ax.axes_bounds=[0 0 1 2/3]
  xlabel(_("Time (sec)"));
  ylabel(_("Frequency (Hz)"));
  zlabel(_("Energy"));
  ax.tight_limits = "on";
 
  set(fig,"user_data",fig_ud)
  SA_SetCmap(fig_ud.cmap)
  SA_compute(fig);
  SA_draw(fig);
  fig.immediate_drawing = idraw;
  SA_gui(fig)
  delmenu(fig.figure_id,_("Control"))
  addmenu(fig.figure_id,_("Control"),[_("Hide");_("Show");_("Adjust")],list(2,"SA_ControlMenu"))
endfunction

