//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [kTe,typ]=decide_n(Tval)
  ntv=size(Tval,'*')
  //si le max se situe en 1 avec une dérivée negative (segment ST decroissant), chercher le premier min local qui suit, puis
  //la fin de la remontée de la courbe (max local ou asymptote, ou extremité). s'il n'y
  //a pas vraiment de remontée (courbe en L) sel102_2 
  //    prendre le point d'inflexion?, le passage a la valeur 50?
  
  ///si le max se situe en 1 avec une dérivée positive (segment ST croissant), chercher la fin
  //de la remontée de la courbe (max local ou asymptote la plus pres de 0), prendre plutot
  //le point au plus tard si sommet en plateau?
  //
  
    //recherche des extrema s'ils existent
  nf=5;nfs2=floor(nf/2);
  if ntv>nf then  
    //dTval=sgolaydiff(Tval,1,3,nf);
    dTval=diff(Tval)
    maxind=find(dTval(1:$-1)>0&dTval(2:$)<0);
    maxindinv=find(dTval(1:$-1)<0&dTval(2:$)>0);
  else
    maxind=[];maxindinv=[];
  end
  if maxind==[] then //pas de max local
    [dum, maxind] = max(Tval);
    if maxindinv==[] then
      [duminv, maxindinv] = max(-Tval);
      if  duminv<dum then //onde p
        kTe=maxind
        typ='p'
      else //onde n
        kTe=maxindinv
        typ='n'
      end
    else//onde n
      
      kTe=maxindinv
      typ='n'
    end
  else // max local
    [dum,k]=max(Tval(maxind))
    maxind=maxind(k)
    if maxindinv==[] then //(pas de min local) onde p
      kTe=maxind
      typ='p'
    else //min local et max local
      [duminv, k] = max(-Tval(maxindinv));
      maxindinv=maxindinv(k)
      s=2.4;
      if dum>s*duminv then //onde p
        kTe=maxind
        typ='p'
      elseif duminv>s*dum then //onde n
        kTe=maxindinv
        typ='n'
      elseif maxindinv<maxind then //onde np
        kTe=maxind
        typ='np'
      else //onde pn
        kTe=maxindinv
        typ='pn'
      end
    end
  end
  kTe=kTe + leftbound - 1
  traceD()
endfunction
