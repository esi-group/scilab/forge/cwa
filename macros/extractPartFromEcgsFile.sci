//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function S=extractPartFromEcgsFile(filein,from,N,sigsel,silent)
  if argn(2)<3 then
    error(msprintf(_("%s: Wrong number of input argument(s): %d to %d expected.\n"),"extractPartFromEcgsFile",3,5))
  end
  u=mopen(filein,"rb");
  if ascii(mget(9,'c',u))<>"ECGScilab" then
     mclose(u)
     error(msprintf(_("%s: Wrong value for input argument #%d: Unexpected file type"),"extractPartFromEcgsFile",1))
  end
  
  t0=mget(6,"s",u);
  Nchannels=mget(1,'i',u);
  Nsamples=mget(1,'i',u);
  fe=mget(1,'f',u);
  if argn(2)<5  then silent=%f;end
  if argn(2)<4|or(size(sigsel)==-1) then 
    sigsel=1:Nchannels;
  else
    if type(sigsel)<>1|~isreal(sigsel) then
      mclose(u)
      error(msprintf(_("%s: Wrong type for argument %d: Real vector expected.\n"),"extractPartFromEcgsFile",4))
    end
    if or(sigsel<1|sigsel>Nchannels) then
      mclose(u)
      error(msprintf(_("%s: Wrong value for input argument #%d: Elements must be in [%d %d].\n"),"extractPartFromEcgsFile",4,1,Nchannels))
    end
 
  end
 
  //Boucle sur les blocs
  pos=mtell(u);
 
  if type(from)<>1|~isreal(from) |size(from,'*')<>1 then
    mclose(u)
    error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),"extractPartFromEcgsFile",2))
  end
  if from<1|from>Nsamples then
     mclose(u)
     error(msprintf(_("%s: Wrong value for input argument #%d: Must be in [%d %d].\n"),"extractPartFromEcgsFile",2,1,Nsamples))
  end
  if type(N)<>1|~isreal(N) |size(N,'*')<>1 then
    mclose(u)
    error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),"extractPartFromEcgsFile",3))
  end
  if N<=0|N>Nsamples-from then
    mclose(u)
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be in [%d %d].\n"),"extractPartFromEcgsFile",3,1,Nsamples-from))
  end
  if from>1 then
    //skip the first samples
    mseek_big(pos+(from-1)*Nchannels*8,u);
  end
 
  if ~silent then winH=mywaitbar("open",_("Processing data acquisition"));end
 
  S=zeros(N,size(sigsel,'*'));

  for k=1:N
    s=matrix(mget(Nchannels,'d',u),1,Nchannels);
    S(k,:)=s(sigsel);
    if ~silent then mywaitbar("update",k/N,winH);end
  end
  mclose(u)
  S=sciecg(fe,S);
   if ~silent then mywaitbar("close",winH);end
endfunction
