function ECGViewerMenus(k,fig_id,varargin)
  //varargin used for setting parameters without queryinfg the user
  if type(k)==1 then
    menu=ECGViewerMenuNames(k)
  else
    menu=k
  end
  fig=get_figure_handle(fig_id)
  ud=fig.user_data;
  fe=ud.fe;
  s=fe;
  mn=60*fe
  h=3600*fe;
  interactive=size(varargin)==0
  select menu
  
  case _("Show info") then //
    messagebox([_("Sampling rate: ")+string(ud.fe)
                _("Number of channels: ")+msprintf("%d",ud.Nchannels)
                _("Number of samples: ")+msprintf("%d",ud.Nsamples)
                _("Initial date: ")+strcat(string(ud.t0),";")]);
    
  case _("Select channels") then
    if interactive then
      [ok,sel]=getvalue(menu,"Channels index [1 "+string(ud.Nchannels)+"]",...
                        list("vec",-1),"1:"+sci2exp(ud.Nchannels,0))
      if ok then ud.Channels=sel;set(fig,"user_data",ud);end 
    else
      ud.Channels=varargin(1);set(fig,"user_data",ud);
    end
  case _("Set interval length") then
    if interactive then
      [ok,n]=getvalue(menu,_("Block length"),list("vec",1),string(ud.BlockLength))
      if ok then ud.BlockLength=max(n,10);set(fig,"user_data",ud);end
    else
      ud.BlockLength=max(varargin(1),10);
      set(fig,"user_data",ud)
    end
  case _("Set start index") then
    if interactive then
      [ok,n]=getvalue(menu,"Index [1 "+string(ud.Nsamples-ud.BlockLength)+"]",...
                      list("vec",1),string(ud.Index))
      if ok then 
        ud.Index=min(max(1,n),ud.Nsamples-ud.BlockLength); 
        set(fig,"user_data",ud)
      end  
    else
      ud.Index=min(max(1,varargin(1)),ud.Nsamples-ud.BlockLength); 
      set(fig,"user_data",ud)
    end

  case _("Set options") then
    if ud.LOC==[] then
      if interactive then
        while %t
          [ok,n,sel]=getvalue(menu,_("Show Synthetic signal 0/1"),list("vec",1),string(ud.Synthetic))
          if ~ok then break,end
          if or(n==[0 1]) then
            ud.Synthetic=n; set(fig,"user_data",ud);break
          end 
        end
      else
        ud.Synthetic=varargin(1);set(fig,"user_data",ud);
      end
      set(ud.ax2,"visible","off")
      set(ud.ax1,"axes_bounds",[0 0 1 1])
    else
      if interactive then
        D=["Rp" "Re" "To" "Tp" "Te" "Po" "Pp" "Pe"  "Qo"];
        Rp= 1; Re=2; To=3;Tp=4;Te=5;Po=6;Pp=7;Pe=8;Qo=9;
        Sn=["QoQo" ,"QoTe","TpTe" "RpRp"];
        QoQo=1;QoTe=2;TpTe=3;RpRp=4;
        selD=ud.Detections
        selS=ud.Segments;
        while %t
          [ok,n,selD,selS]=getvalue(menu,[_("Show Synthetic signal 0/1");
                    _("Select detections ")+strcat(D,", ") 
                    _("Select segments ")+strcat(Sn,", ")],...
                                    list("vec",1,"vec",-1,"vec",-1),...
                                    [string(ud.Synthetic);
                    "["+strcat(D(selD),' ')+"]";
                    "["+strcat(Sn(selS),' ')+"]"]);
          
          if ~ok then break,end
          if and(selD>0&selD<=9)&or(n==[0 1])& and(selS>0&selS<=4) then
            ud.Detections=selD;
            ud.Synthetic=n
            ud.Segments=selS
            set(fig,"user_data",ud);
            break;
          end 
        end
      else
        ud.Synthetic=varargin(1);
        ud.Detections=varargin(2);
        ud.Segments=varargin(3);
        set(fig,"user_data",ud);
      end
    end
  case _("Show") then
    fig.event_handler_enable="off"
    scf(fig);
    exec(ECGViewerGetData,-1);
    drawlater()
    ax1=ud.ax1;
    ax2=ud.ax2;
    if ud.Segments<>[] then 
      set(ax2,"visible","on");
      set(ax1,"axes_bounds",[0 0 1 0.5])
    else
      set(ax2,"visible","off");
      set(ax1,"axes_bounds",[0 0 1 1])
    end
    sca(ax1);ax1.zoom_box=[];
    if ax1.children<>[] then delete(ax1.children);end
    t=(init-1+(0:size(S,1)-1))/ud.fe;
    plot2d(t,S);
    ax1.data_bounds=[t(1) min(S);t($) max(S)];
    if ud.Synthetic==1 then
      Ss=SyntheticECG(S)
      plot2d(t,Ss);e=gce();e.children.foreground=color("lightgray");
    else
      Ss=S(:,$);
    end
    if ud.LOC<>[] then
      //Draw detected locations
      //get index of the first detection in the current part of the signal
      [m,kd]=min(abs(ud.LOC(9,:)-init));
      if ud.LOC(9,kd)<init then kd=kd+1;end
      //get index of the last detection index in the current part of thesignal
      [m,kf]=min(abs(ud.LOC(9,:)-init-size(S,1)));
      if ud.LOC(9,kf)>=init+size(S,1) then kf=kf-1;;end

      loc=ud.LOC(:,kd:kf);
      for d=matrix(ud.Detections,1,-1)
        sel=loc(d,:)-init+1;sel=sel(sel>0);
        xpoly(t(sel)',Ss(sel),"marks");
        ECGSetMarks(gce(),d)
      end
      if ud.Segments<>[] then
        ax2=ud.ax2;ax2.zoom_box=[];
        if ax2.children<>[] then delete(ax2.children);end
        sca(ax2);
        Sn=["QoQo","QoTe","TpTe","RpRp"];
        COLORS=["m","b","c","r"];
        mn=%inf;mx=-%inf;
        for selS=ud.Segments
          Res=[];tt=[];
          select selS
          case 1 //QoQo
            kQo=loc(9,:);
            Res=diff(kQo)*(1000/ud.fe);
            plot(t(kQo(1:$-1)-init+1),Res,COLORS(selS))
            mn=min(mn,min(Res));
            mx=max(mx,max(Res));
          case 2 //QoTe
            Ic=ECGContiguousDetections(loc,5)
            for i=1:size(Ic,1)
              kd1=Ic(i,1);
              kf1=Ic(i,2);
              kQo=loc(9,kd1:kf1-1);
              kTe=loc(5,kd1+1:kf1-1);
              tt=[tt,t(kQo(1:$-1)-init+1), %nan ];
              Res=[Res,(kTe-kQo(1:$-1))*(1000/ud.fe),%nan];
            end
             if tt<>[] then 
               plot(tt,Res,COLORS(selS));
               mn=min(mn,min(Res));
               mx=max(mx,max(Res));
             end
          case 3 //TpTe
            Ic=ECGContiguousDetections(loc,[4 5])
            for i=1:size(Ic,1)
              kd1=Ic(i,1);
              kf1=Ic(i,2);
              kQo=loc(9,kd1:kf1-1);
              kTp=loc(4,kd1+1:kf1-1);
              kTe=loc(5,kd1+1:kf1-1);
              tt=[tt,t(kQo(1:$-1)-init+1),%nan ];
              Res=[Res,(kTe-kTp)*(1000/ud.fe), %nan];
            end
            if tt<>[] then 
              plot(tt,Res,COLORS(selS));
              mn=min(mn,min(Res));
              mx=max(mx,max(Res));
            end
          case 4 //RpRp
            kQo=loc(9,:);
            kRp=loc(1,:);
            Res=diff(kRp)*(1000/ud.fe)
            plot(t(kQo(1:$-1)-init+1),Res,COLORS(selS))
            mn=min(mn,min(Res));
            mx=max(mx,max(Res));
          end
        end
        ax2.data_bounds=[ax1.data_bounds(:,1),[mn;mx]];
        legend(Sn(ud.Segments));
      end
    end
    sca(ax1);
    drawnow()
  case _("Save") then
    F=uiputfile("*")
    if F<>"" then 
      P=fig.children(1).children($).children
      S=[];
      for k=1:size(P,'*')
        S=[S P($+1-k).data(:,2)];
      end
      //exec(ECGViewerGetData,-1);
      save(F,"S")
    end
  end
endfunction
function ECGViewerGetData()
  init=ud.Index;
  N=ud.BlockLength
  sel=ud.Channels
  u=ud.fid
  if u<>[] then
    pos=ud.EOH+(init-1)*ud.Nchannels*8
    mseek_big(pos,u)
    S=matrix(mget(ud.Nchannels*N,'d',u),ud.Nchannels,-1)';
    S = S(:,sel);
  else
    last=min(init-1+N,size(ud.sigs,1));
    S=ud.sigs(init:last,sel)
  end
endfunction
