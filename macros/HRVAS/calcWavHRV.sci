function output=calcWavHRV(f,power,Scale,Cdelta,var,n,dj,dt,VLF,LF,HF)
//calcAreas - Calulates areas/energy under the PSD curve within the freq
//bands defined by VLF, LF, and HF. Returns areas/energies as ms^2,
//percentage, and normalized units. Also returns LF/HF ratio.
//
//Inputs:
//   PSD: PSD vector
//   F: Freq vector
//   VLF, LF, HF: array containing VLF, LF, and HF freq limits
//   flagNormalize: option to normalize PSD to max(PSD)
//Output:
//
//Usage:
//   

    // Scale-average between VLF, LF, and HF bands/scales
    //f=fliplr(f); //put f in it's original order
    iVLF = find((f >= VLF(1)) & (f < VLF(2)));
    iLF = find((f >= LF(1)) & (f < LF(2)));
    iHF = find((f >= HF(1)) & (f < HF(2)));
    scale_avg = (Scale')*(ones(1,n));  // expand scale --> (J+1)x(N) array
    scale_avg = power ./ scale_avg;   // [Eqn(24)]
    vlf_scale_avg = var*dj*dt/Cdelta*sum(scale_avg(iVLF,:),1);   // [Eqn(24)]
    lf_scale_avg = var*dj*dt/Cdelta*sum(scale_avg(iLF,:),1);
    hf_scale_avg = var*dj*dt/Cdelta*sum(scale_avg(iHF,:),1);
    
     // calculate raw areas (power under curve), within the freq bands (ms^2)
    output.aVLF=vlf_scale_avg;
    output.aLF=lf_scale_avg;
    output.aHF=hf_scale_avg;
    output.aTotal=output.aVLF+output.aLF+output.aHF;
        
    //calculate areas relative to the total area (%)
    output.pVLF=(output.aVLF./output.aTotal)*100;
    output.pLF=(output.aLF./output.aTotal)*100;
    output.pHF=(output.aHF./output.aTotal)*100;
    
    //calculate normalized areas (relative to HF+LF, n.u.)
    output.nLF=output.aLF./(output.aLF+output.aHF);
    output.nHF=output.aHF./(output.aLF+output.aHF);
    
    //calculate LF/HF ratio
    output.LFHF =output.aLF./output.aHF;
    ieee_s=ieee();ieee(2)
    output.rLFHF=sum(output.LFHF>1)/sum(output.LFHF<=1);
    ieee(ieee_s)
    //calculate peaks
    output.peakVLF=zeros(vlf_scale_avg);
    output.peakLF=output.peakVLF;
    output.peakHF=output.peakVLF;
    
endfunction
