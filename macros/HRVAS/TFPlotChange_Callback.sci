function TFPlotChange_Callback(m)
// Callback function run when time-freq plot type change
  c=gcbo();while c.type<>"Figure" then c=c.parent;end
  ud=c.user_data;
  h=ud.h
  // If waterfall plot is selected disable Wavelet option.
  // Waterfall plot takes too long for wavlet b'c it contains many
  // time values.
  if %f
  pt=get(h.listTFPlot,'string');
  pt=convstr(pt(get(h.listTFPlot,'value')));
  if pt=='waterfall' then
    if get(h.radioTFPlotWav,'value') // if selected change selection
      set(h.radioTFPlotAR,'value',1)
      warning('Can not currently plot waterfall using wavelet transforms.')
    end
    set(h.radioTFPlotWav, 'enable','off');
  else
    set(h.radioTFPlotWav, 'enable','on');
  end
  end
  if ud.HRV<>[] then
    drawlater
    plotTF(h,ud.HRV,ud.settings);     
    drawnow
  end
endfunction
