function tH = createTFTbl(aH)   
  x=[0.05 0.28+0.145*(0:4) ];nx=size(x,'*');                                                    
  aH.data_bounds=[0 0;1 1];
  H=0.055
  y=(1-2*H)-(1:16)*H     

  //Horizontal Lines
  xsegs([0 1],[1 1]*(1-1.5*H))
      
  //Header
   hn=['Frequency band','Peak(Hz)','Power(ms^2)','Power(%)','Power(n.u.)','LF/HF(ratio)']
     
  for l=1:size(hn,'*')
    xstring(x(l),1-1.5*H,hn(l))
  end
  //Column 1
  tH=gce();
  Vn=['Burg PSD','Lomb-Scargle PSD','Wavelet PSD',"SPWV"];
  vn=['VLF' 'LF','HF']
  i=0
  fs1=0.8;
  fs2=0.7;
  for L=1:size(Vn,'*')
    i=i+1
    xstring(0.01,y(i),Vn(L));e=gce();e.font_size=fs1;e.font_style=8;tH(i,1)=e;
    for k=2:nx
      xstring(x(k),y(i),"");e=gce();e.font_size=fs2;tH(i,k)=e;
    end
  
    for l=1:size(vn,'*')
      i=i+1
      xstring(x(1),y(i),vn(l));e=gce();e.font_size=fs2;tH(i,1)=e;
      for k=2:nx
        xstring(x(k),y(i),"0.0");e=gce();e.font_size=fs2;tH(i,k)=e;
      end
    end
  end
  tH([2 6 10 14],5).text=""
  tH([4 8 12 16],6).text=""
  
 
endfunction
