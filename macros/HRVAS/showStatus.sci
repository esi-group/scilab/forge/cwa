function showStatus(str)
  if h==[] then return;end
  st=h.lblStatus
  if argn(2) < 1|stripblanks(str)=="" then 
    st.visible="off"
    st.children.string=""
  else
    Main=st.user_data
    st.position(1:2)=Main.figure_position
    st.children.string=str+": Please wait";
    st.visible="on"
  end
endfunction
