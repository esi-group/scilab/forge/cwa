function v=isvalid(h,mn,mx,typ)
  CB=h.Callback;h.Callback=""
  [v,ierr]=evstr(get(h,'string'));
  ok=ierr==0
  if ok&or(typ==["int","odd","even"]) then ok=(round(v)==v);end
  if ok&typ=="odd" then ok=modulo(v,2)==1;end
  if ok&typ=="even" then ok=modulo(v,2)==0;end
  ok=ok&v>=mn&v<=mx
  if ~ok then
    h.BackgroundColor=[1 0 0];
  elseif and(h.BackgroundColor==[1 0 0]) then
    if h.user_data==[] then
      h.BackgroundColor=[1 1 1];
    else
      h.BackgroundColor=h.user_data;
    end
  end
  h.Callback=CB
endfunction
