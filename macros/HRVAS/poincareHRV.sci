function output=poincareHRV(ibi)
//poincareHRV(ibi) - calculates poincare HRV
//
//Inputs:    ibi = 2dim array containing [t (s),ibi (s)]
//           
//Outputs:   output is a structure containg HRV.

    //check inputs
    //assumes ibi units are seconds
    
    sd=diff(ibi(:,2)); //successive differences
    rr=ibi(:,2);
    SD1=sqrt( 0.5*stdev(sd)^2 );
    SD2=sqrt( 2*(stdev(rr)^2) - (0.5*stdev(sd)^2) );
    
    //format decimal places
    output.SD1=round(SD1*10)/10; //ms
    output.SD2=round(SD2*10)/10; //ms

endfunction
