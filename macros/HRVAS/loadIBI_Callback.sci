function loadIBI_Callback()
  c=gcbo();while c.type<>"Figure" then c=c.parent;end
  ud=c.user_data;
  h=ud.h
  filename = uigetfile( ...
      ['*.ibi;*.rr;*.txt','IBI Files (*.ibi,*.rr,*.txt)';
       '*.ibi',  'IBI (*.ibi)'; ...
       '*.rr','RR (*.rr)'; ...
       '*.txt','Text (*.txt)'; ...
       '*.*',  'All Files (*.*)'], ...
      'Select IBI data file',...
      "");
        
  if filename=="" then
    return //user selected cancel
  else
    ibi=loadIBI(filename)
    if ibi<>[] then
      h=ud.h
      ud.IBI=ibi
      ud.nIBI=[]
      ud.dIBI=[]
      ud.trend=[]
      ud.HRV=[]
      h.menuRun.enable="on"
      set(c,'user_data',ud)
    end
  end

endfunction
