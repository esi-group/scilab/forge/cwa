The HRVAS tool for Scilab has been derived from the Matlab version
developped by 

    John T. Ramshur
    University of Memphis
    Department of Biomedical Engineering
    Memphis, TN
    jramshur@gmail.com
    jramshur@memphis.edu

and published under the  GNU General Public License:

    Copyright (C) 2010, John T. Ramshur

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Main modifications
------------------
The Scilab version is not an exact clone of the Matlab version:

  - The menu for batch processing has been replaced by a batch mode
    which allow to chain several analyzes at the Scilab language level

  - Smoothed pseudo Wigner Ville method added for Time frequency domain
    analysis

  - Graphical interface user settings are checked for validity.

  - The Matlab version global variables are stored in the user_data
    field of the HRVAS main figure. So it is possible to have several
    instances of HRVAS running.

  - waterfall drawing of PSD has been removed
 

