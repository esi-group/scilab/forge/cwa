function area(varargin)
  //partial emulation of the Matlab area function
  knum=size(varargin)
  for k=1:size(varargin)
    if and(type(varargin(k))<>[1 9]) then knum=k-1;break;end
  end
  facecolor=[]
  for k=knum+1:2:size(varargin)
    if convstr(varargin(k))=="facecolor" then 
      facecolor=addcolor(varargin(k+1));
      varargin(k+1)=null()
      varargin(k)=null()
      break;
    end
  end
  level=0
  if type(varargin(1))<>9 then
    if knum==3 then 
      level=varargin(knum)
      varargin(knum)=null()
      knum=knum-1;
    elseif knum==2&size(varargin(knum),'*')==1 then
      level=varargin(knum)
      varargin(knum)=null()
      knum=knum-1;
    end
  else
    if knum==4 then 
      level=varargin(knum)
      varargin(knum)=null()
      knum=knum-1;
    elseif knum==3&size(varargin(knum),'*')==1 then
      level=varargin(knum)
      varargin(knum)=null()
      knum=knum-1;
    end

  end
y=varargin(knum)
if and(size(y)>1) then 
  y=cumsum(y,2);
  varargin(knum)=y(:,$:-1:1)
end
fig=gcf();
if fig.immediate_drawing=="on" then
  fig.immediate_drawing="off"
  id_change=%t
else
  id_change=%f
end
//drawlater()
plot(varargin(:))
e=gce();p=e.children;ax=e.parent
np=size(p,'*');

for k=1:size(p,'*')
  pk=p(k);
  pk.data=[[pk.data(1,1),level];pk.data;[pk.data($,1),level]]
  pk.closed = "on"
  pk.fill_mode = "on"
  if facecolor<>[] then
    pk.background=facecolor
    pk.foreground=-1
  else
    pk.background=pk.foreground
    pk.foreground=-1
  end
end

ax.data_bounds(1,2)=min(level,ax.data_bounds(1,2))
if id_change then fig.immediate_drawing="on";end

endfunction
