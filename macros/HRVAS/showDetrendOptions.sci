function showDetrendOptions
// showDetrendOptions: show/hide detrending options in GUI
  selection=get(h.listDetrend,'Value');
  if selection==2 //Wavelet
//    set(h.listSmoothMethod,'Visible','off');
//    set(h.lblSmoothMethod,'Visible','off');
    
    set(h.txtSmoothLoessAlpha,'Visible','off');
    set(h.lblSmoothLoessAlpha,'Visible','off');
    set(h.txtSmoothLoessOrder,'Visible','off');
    set(h.lblSmoothLoessOrder,'Visible','off');
    set(h.txtSmoothSgolayLen,'Visible','off');
    set(h.lblSmoothSgolayLen,'Visible','off');
    set(h.txtSmoothSgolayDegree,'Visible','off');
    set(h.lblSmoothSgolayDegree,'Visible','off');        
    
    set(h.lblPoly,'Visible','off');
    set(h.listPoly,'Visible','off');
    set(h.listWaveletType,'Visible','on');
    set(h.lblWaveletType,'Visible','on');
    set(h.txtWaveletType2,'Visible','on');
    set(h.txtWaveletLevels,'Visible','on');
    set(h.lblWaveletLevels,'Visible','on');
    set(h.txtPriorsFc,'Visible','off');
    set(h.lblPriorsFc,'Visible','off');
  elseif selection==3 //Stavisky Golay
    set(h.lblPoly,'Visible','off');
    set(h.listPoly,'Visible','off');
    set(h.listWaveletType,'Visible','off');
    set(h.lblWaveletType,'Visible','off');
    set(h.txtWaveletType2,'Visible','off');
    set(h.txtWaveletLevels,'Visible','off');
    set(h.lblWaveletLevels,'Visible','off');
    set(h.txtPriorsFc,'Visible','off');
    set(h.lblPriorsFc,'Visible','off');
    set(h.txtSmoothLoessAlpha,'Visible','off');
    set(h.lblSmoothLoessAlpha,'Visible','off');
    set(h.txtSmoothLoessOrder,'Visible','off');
    set(h.lblSmoothLoessOrder,'Visible','off');
    set(h.txtSmoothSgolayLen,'Visible','on');
    set(h.lblSmoothSgolayLen,'Visible','on');
    set(h.txtSmoothSgolayDegree,'Visible','on');
    set(h.lblSmoothSgolayDegree,'Visible','on');
  elseif selection==4 //Loess
    set(h.lblPoly,'Visible','off');
    set(h.listPoly,'Visible','off');
    set(h.listWaveletType,'Visible','off');
    set(h.lblWaveletType,'Visible','off');
    set(h.txtWaveletType2,'Visible','off');
    set(h.txtWaveletLevels,'Visible','off');
    set(h.lblWaveletLevels,'Visible','off');
    set(h.txtPriorsFc,'Visible','off');
    set(h.lblPriorsFc,'Visible','off');
    set(h.txtSmoothLoessAlpha,'Visible','on');
    set(h.lblSmoothLoessAlpha,'Visible','on');
    set(h.txtSmoothLoessOrder,'Visible','on');
    set(h.lblSmoothLoessOrder,'Visible','on');
    set(h.txtSmoothSgolayLen,'Visible','off');
    set(h.lblSmoothSgolayLen,'Visible','off');
    set(h.txtSmoothSgolayDegree,'Visible','off');
    set(h.lblSmoothSgolayDegree,'Visible','off');
  elseif selection==5 //poly detrend
    set(h.txtSmoothLoessAlpha,'Visible','off');
    set(h.lblSmoothLoessAlpha,'Visible','off');
    set(h.txtSmoothLoessOrder,'Visible','off');
    set(h.lblSmoothLoessOrder,'Visible','off');
    set(h.txtSmoothSgolayLen,'Visible','off');
    set(h.lblSmoothSgolayLen,'Visible','off');
    set(h.txtSmoothSgolayDegree,'Visible','off');
    set(h.lblSmoothSgolayDegree,'Visible','off');    
    
    set(h.listWaveletType,'Visible','off');
    set(h.lblWaveletType,'Visible','off');
    set(h.txtWaveletType2,'Visible','off');
    set(h.txtWaveletLevels,'Visible','off');
    set(h.lblWaveletLevels,'Visible','off');
    set(h.txtPriorsFc,'Visible','off');
    set(h.lblPriorsFc,'Visible','off');
    set(h.lblPoly,'Visible','on');
    set(h.listPoly,'Visible','on');
    
  elseif selection==6 //Smoothness Priors
//    set(h.listSmoothMethod,'Visible','off');
//    set(h.lblSmoothMethod,'Visible','off');
    
    set(h.txtSmoothLoessAlpha,'Visible','off');
    set(h.lblSmoothLoessAlpha,'Visible','off');
    set(h.txtSmoothLoessOrder,'Visible','off');
    set(h.lblSmoothLoessOrder,'Visible','off');
    set(h.txtSmoothSgolayLen,'Visible','off');
    set(h.lblSmoothSgolayLen,'Visible','off');
    set(h.txtSmoothSgolayDegree,'Visible','off');
    set(h.lblSmoothSgolayDegree,'Visible','off');    

    set(h.lblPoly,'Visible','off');
    set(h.listPoly,'Visible','off');
    set(h.listWaveletType,'Visible','off');
    set(h.lblWaveletType,'Visible','off');
    set(h.txtWaveletType2,'Visible','off');
    set(h.txtWaveletLevels,'Visible','off');
    set(h.lblWaveletLevels,'Visible','off');
    set(h.txtPriorsFc,'Visible','on');
    set(h.lblPriorsFc,'Visible','on');
  else //None
//    set(h.listSmoothMethod,'Visible','off');
//    set(h.lblSmoothMethod,'Visible','off');

    set(h.txtSmoothLoessAlpha,'Visible','off');
    set(h.lblSmoothLoessAlpha,'Visible','off');
    set(h.txtSmoothLoessOrder,'Visible','off');
    set(h.lblSmoothLoessOrder,'Visible','off');
    set(h.txtSmoothSgolayLen,'Visible','off');
    set(h.lblSmoothSgolayLen,'Visible','off');
    set(h.txtSmoothSgolayDegree,'Visible','off');
    set(h.lblSmoothSgolayDegree,'Visible','off');

    set(h.lblPoly,'Visible','off');
    set(h.listPoly,'Visible','off');
    set(h.listWaveletType,'Visible','off');
    set(h.lblWaveletType,'Visible','off');
    set(h.txtWaveletType2,'Visible','off');
    set(h.txtWaveletLevels,'Visible','off');
    set(h.lblWaveletLevels,'Visible','off');
    set(h.txtPriorsFc,'Visible','off');
    set(h.lblPriorsFc,'Visible','off');
  end
endfunction
