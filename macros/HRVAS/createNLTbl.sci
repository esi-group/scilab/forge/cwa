//// GUI: CreateNLTbl
function tH = createNLTbl(aH)
  aH.data_bounds=[0 0;1 1];
  H=0.1
  x=[0.05 0.45 0.8]//relative x position of cols
  y=(1-3*H)-(1:6)*H      
 //Horizontal Lines
  xstring(0.4,1-H*1.25,'Non linear statistics');e=gce();
  xsegs([0 1],[1 1]*(1-1.25*H))
  xsegs([0 1],[1 1]*(1-2.5*H))
 //Col Headers
  hn=['Variables','Units','Values'];
  for l=1:size(hn,'*')
    xstring(x(l),1-2.75*H,hn(l))
  end
  //Column 1
  tH=gce();
  vn=['Entropy' '$\phantom{XXX}\Large SampEn$' 'DFA' '$\phantom{XXX}\Large\alpha_{all}$' '$\phantom{XXX}\Large\alpha_{1}$' '$\phantom{XXX}\Large\alpha_{2}$'];
  u=['' '-' '' '-' '-' '-'];
  v=['' '0.0' '' '0.0' '0.0' '0.0'];
   for l=1:size(vn,'*')
    xstring(x(1),y(l),vn(l));e=gce();e.font_size=0.8;tH(l,1)=e;
   if l==5 then e.font_foreground=color('red');end
   if l==6 then e.font_foreground=color('green');end
    xstring(x(2),y(l),u(l));e=gce();e.font_size=0.8;e.alignment='right';tH(l,2)=e;
    xstring(x(3),y(l),v(l));e=gce();e.font_size=0.8;e.alignment='right';tH(l,3)=e;
  end

endfunction
