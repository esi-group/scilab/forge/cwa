function ibi=loadIBI(f,opt)
// loadIBI: function to load ibi data file into array    
  ibi=[]; 
  f=stripblanks(f)
  k=find(f=="");if k<>[] then f(k)=[];end
  if f==[]|f=="" then return;end
  if isfile(f)==[] then
    messagebox('Error opening file: '+f,"modal")
    return
  end                

  ibi=fscanfMat(f)
  //check and convert IBI to milli seconds
  ibi=checkIBI(ibi)
  
endfunction
