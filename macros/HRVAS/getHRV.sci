function [output,dIBI,nIBI,trend,art]=getHRV(h,IBI,settings)
// getHRV: computes all HRV
//
// Inputs:
//   f: file name of ibi file
//   settings: structure containing all hrv analysis options
// Outputs:
//   output: structure containing all hrv results
//
// NOTE: If you change getHRV or detrendIBI functions here you will need
// to copy the changes over to the batchHRV module to use batch
// proccessing
 
  with_GUI=h<>[];
  nIBI=[]; dIBI=[];

  ////////////////////////////////////////////////////////////////////////////////////////////
  //Preprocess Data
  ////////////////////////////////////////////////////////////////////////////////////////////
  showStatus("Preprocessing");

  //build cell array of locate artifacts methods
  methods=[]; methInput=[];
  if settings.ArtLocatePer
    methods=[methods,"percent"];
    methInput=[methInput,settings.ArtLocatePerVal];
  end
  if settings.ArtLocateSD
    methods=[methods,"sd"];
    methInput=[methInput,settings.ArtLocateSDVal];
  end
  if settings.ArtLocateMed
    methods=[methods,"median"];
    methInput=[methInput,settings.ArtLocateMedVal];
  end
  //determine which window/span to use

  if convstr(settings.ArtReplace)=="mean" then
    replaceWin=settings.ArtReplaceMeanVal;
  elseif convstr(settings.ArtReplace)=="median" then
    replaceWin=settings.ArtReplaceMedVal;
  else
    replaceWin=0;
  end

  //Note: We don't need to use all the input arguments,but we will let the
  //function handle all inputs
  [dIBI,nIBI,trend,art] = preProcessIBI(IBI, ...
                                        "locateMethod", methods, "locateInput", methInput, ...
                                        "replaceMethod", settings.ArtReplace, ...
                                        "replaceInput",replaceWin, "detrendMethod", settings.Detrend, ...
                                        "smoothLoessAlpha", settings.SmoothLoessAlpha, ...
                                        "smoothLoessOrder", settings.SmoothLoessOrder, ...
                                        "smoothSgolayLen", settings.SmoothSgolayLen, ...
                                        "smoothSgolayDegree", settings.SmoothSgolayDegree, ...
                                        "polyOrder", settings.PolyOrder, ...
                                        "waveletType", settings.WaveletType, ...
                                        "waveletLevels", settings.WaveletLevels, ...
                                        "fc", settings.PriorsFc,...
                                        "resampleRate",settings.Interp,...
                                        "meanCorrection",%t);

  if with_GUI then
    if sum(art)>0
      set(h.lblArtLocate,"string", ...
          msprintf("Ectopic Detection [%.2f%s]",sum(art)/size(IBI,1)*100),"%")
    else
      set(h.lblArtLocate,"string","Ectopic Detection")
    end

    //Plot IBI
    plotIBI(h,settings,IBI,dIBI,nIBI,trend,art);
  end
  output.name="";
 
  //Time-Domain (using non-detrented ibi)
  showStatus("Time Domain Analysis");
  //%tda_i_st=generic_i_st;
  output.time=timeDomainHRV(nIBI(:,1),nIBI(:,2),settings.SDNNi*60,settings.pNNx);

  //Freq-Domain
  showStatus("Frequency Domain Analysis");
  output.freq=freqDomainHRV(dIBI,settings.VLF,settings.LF,settings.HF,settings.AROrder,...
                            settings.WinWidth,settings.WinOverlap,settings.Points,settings.Interp);

  //Nonlinear (using non-detrented ibi)
  showStatus("Nonlinear Analysis");
  output.nl=nonlinearHRV(nIBI,settings.m,settings.r,settings.nRange,settings.breakpoint);
  //Time-Freq
  showStatus("Time Frequency Analysis");
  output.tf=timeFreqHRV(dIBI,nIBI,settings.VLF,settings.LF,settings.HF,settings.AROrder, ...
                        settings.tfWinSize,settings.tfOverlap, ...
                        settings.Points,settings.Interp,settings.tftsw,settings.tffsw,...
                        ["ar","lomb","wavelet","spwv"]);
 

  showStatus("");
endfunction
