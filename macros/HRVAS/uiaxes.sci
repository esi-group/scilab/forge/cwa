function h=uiaxes(varargin)
  keys=[]
  for k=1:2:size(varargin)
    keys=[keys convstr(varargin(k))];
  end
  kp=2*(find(keys=="parent"))
  h = newaxes(varargin(kp));
  kp=find(keys=="position")
  if kp<>[] then h.axes_bounds=varargin(2*kp);end
  kp=find(keys=="box");
  if kp<>[] then h.box=varargin(2*kp);end
  h.axes_visible='on';
  h.margins=zeros(1,4)
  h.data_bounds=[0 0;1 1];
endfunction
