function [cwtpower,f,Scale,Cdelta,n,dj,dt,var]=calcWavelet_TF(y,fs)
  
  var = stdev(y)^2;
  y = (y - mean(y))/sqrt(var) ;

  n = length(y);
  dt = 1/fs;   
  //xlim = [0,t2($)];  // plotting range
  pad = 1;      // pad the time series with zeroes (recommended)
  dj = 1/64;    // this will do 4 sub-octaves per octave
  s0 = 2*dt;    // this says start at a Scale of 0.5 s
  j1 = 7/dj;    // this says do 7 powers-of-two with dj sub-octaves each
  lag1 = 0.72;  // lag-1 autocorrelation for red noise background

  mother = 'Morlet';
  //mother = 'DOG';
  //mother = 'Paul';
  Cdelta = 0.776;   // this is for the MORLET wavelet
  
  // Wavelet transform
  [wave,period,Scale,coi] = wavelet(y,dt,pad,dj,s0,j1,mother);
  // Reference: Torrence, C. and G. P. Compo, 1998: A Practical Guide to
  // Wavelet Analysis. <I>Bull. Amer. Meteor. Soc.</I>, 79, 61-78.    
  
  cwtpower = (abs(wave)).^2 ; // compute wavelet power spectrum              
  
  f=1.0./period(:,$:-1:1); //frequency in ascending order
  cwtpower=cwtpower($:-1:1,:); //flip to match freq. order
endfunction

