function showToolbar(src,evnt)
// function to show/hide toolbar    
  state=convstr(get(h.MainFigure,'toolbar'));
  if state=='figure' then
    set(h.MainFigure,'toolbar','none')
  else
    set(h.MainFigure,'toolbar','figure')
  end
endfunction
