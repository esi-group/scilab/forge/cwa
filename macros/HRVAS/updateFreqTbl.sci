function updateFreqTbl(h,hrv,opt)
    // updateFreqTbl: updates freq-domain results table with hrv data

        welch=hrv.freq.welch.hrv;
        ar=hrv.freq.ar.hrv;
        lomb=hrv.freq.lomb.hrv;    
        tH=h.text.freq; //handles of text objects
        nowelch=isempty(hrv.freq.welch.psd)
        //column 2         
        if nowelch then
          set(tH(2:4,2),'text',"nd")
        else
          set(tH(2,2),'text',msprintf('%0.2f',welch.peakVLF))
          set(tH(3,2),'text',msprintf('%0.2f',welch.peakLF))
          set(tH(4,2),'text',msprintf('%0.2f',welch.peakHF))
        end
        set(tH(6,2),'text',msprintf('%0.2f',ar.peakVLF))
        set(tH(7,2),'text',msprintf('%0.2f',ar.peakLF))
        set(tH(8,2),'text',msprintf('%0.2f',ar.peakHF))
        set(tH(10,2),'text',msprintf('%0.2f',lomb.peakVLF))      
        set(tH(11,2),'text',msprintf('%0.2f',lomb.peakLF))
        set(tH(12,2),'text',msprintf('%0.2f',lomb.peakHF))

        //Column 3
        if nowelch then
          set(tH(2:4,3),'text',"nd")
        else
          set(tH(2,3),'text',msprintf('%0.1f',welch.aVLF*1e6))
          set(tH(3,3),'text',msprintf('%0.1f',welch.aLF*1e6))
          set(tH(4,3),'text',msprintf('%0.1f',welch.aHF*1e6))
        end
        set(tH(6,3),'text',msprintf('%0.1f',ar.aVLF*1e6))
        set(tH(7,3),'text',msprintf('%0.1f',ar.aLF*1e6))
        set(tH(8,3),'text',msprintf('%0.1f',ar.aHF*1e6))
        set(tH(10,3),'text',msprintf('%0.1f',lomb.aVLF))      
        set(tH(11,3),'text',msprintf('%0.1f',lomb.aLF))
        set(tH(12,3),'text',msprintf('%0.1f',lomb.aHF))
                
        //Column 4
        if nowelch then
          set(tH(2:4,4),'text',"nd")
        else
          set(tH(2,4),'text',msprintf('%0.1f',welch.pVLF))
          set(tH(3,4),'text',msprintf('%0.1f',welch.pLF))
          set(tH(4,4),'text',msprintf('%0.1f',welch.pHF))     
        end
        set(tH(6,4),'text',msprintf('%0.1f',ar.pVLF))
        set(tH(7,4),'text',msprintf('%0.1f',ar.pLF))
        set(tH(8,4),'text',msprintf('%0.1f',ar.pHF))       
        set(tH(10,4),'text',msprintf('%0.1f',lomb.pVLF))      
        set(tH(11,4),'text',msprintf('%0.1f',lomb.pLF))
        set(tH(12,4),'text',msprintf('%0.1f',lomb.pHF))        
       
        //Column 5)
        if nowelch then
          set(tH(3:4,5),'text',"nd")
        else
          set(tH(3,5),'text',msprintf('%0.3f',welch.nLF))
          set(tH(4,5),'text',msprintf('%0.3f',welch.nHF))
        end
        set(tH(7,5),'text',msprintf('%0.3f',ar.nLF))
        set(tH(8,5),'text',msprintf('%0.3f',ar.nHF))
        set(tH(11,5),'text',msprintf('%0.3f',lomb.nLF))
        set(tH(12,5),'text',msprintf('%0.3f',lomb.nHF))
        
        //Column 6
        if nowelch then
          set(tH(2,6),'text',"nd")
        else
          set(tH(2,6),'text',msprintf('%0.3f',welch.LFHF))
        end
        set(tH(6,6),'text',msprintf('%0.3f',ar.LFHF))
        set(tH(10,6),'text',msprintf('%0.3f',lomb.LFHF))
endfunction
