//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function occ=dsearch2D(xy,dx,dy,opt)
//xy, coordinates of the points of the cloud
//dx, discretization of the x axis 
//dy , discretization of the y axis 
//occ, table such as occ(i,j) contains the number of points in the
//"pixel" [dx(i) dx(i+1)) x [dy(j) dy(j+1))
  if argn(2)<4 then opt='c';end
  if and(opt<>["c","d"]) then
    error(msprintf(_("%: unknown char specifier (must be ''c'' or ''d'')\n"),"dsearch2D"))
  end
  if size(xy,2)<>2 then 
    error("Wrong dimension for first argument")
  end
  [indx,occx]=dsearch(xy(:,1),dx,opt);
  occ=[];
  dy=matrix(dy,1,-1);
  for k=1:length(dx)-1
    i=find(indx==k);
    [indy,occy]=dsearch(xy(i,2),dy,opt);
    occ=[occ; occy];
  end
endfunction 
