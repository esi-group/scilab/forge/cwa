// This file is part of the  Cardiovascular Wawes Analysis toolbox
// Copyright (C) 2005 - INRIA - Qinghua Zhang
// Copyright (C) 2014 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [kTe,Ttype] = ECGTwaveEnds(S, fs, Rpeaks,swin, mthrld)
//TWAVEEND:  T-wave end location
// kTe = ECGTwaveEnds(S, fs, Rpeaks,[,swin [,mthrld]])
//
//   S:       ECG signal (single channel)
//   fs:      sampling frequency
//   Rpeaks : R wave peak indices
// Optional input arguments for sliding window width and morphology threshold specification:
//   swin:    is given as the number of sample points.
//   mthrld:  is either a positive number or a character with 'p' for positive
//            T-wave or 'n' for negative T-wave.
//            positive number gives a threshold used to distinguish mono
//            and biphasic waves: if the ratio of the first peak height by
//            the second peak height if greater than mthrld then the T
//            wave is considered to be monophasic.
// Outputs    
//   kTe: T-wave end indices
// References
//  https://www.researchgate.net/publication/224628140_Robust_and_efficient_location_of_T-wave_ends_in_electrocardiogram
//  https://www.researchgate.net/publication/6646590_An_algorithm_for_robust_and_efficient_location_of_T-wave_ends_in_electrocardiograms
  
// Authors: 
//  - Qinghua Zhang (original Matlab function) twaveend.m  part of the
//         ecgtwave package registered at APP 
//         under the number IDDN.FR.001.430041.000.S.P.2005.000.31230. 
//  - Serge Steer (translation to Scilab and vectorization)
  nin = argn(2);
  if nin<3 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),"ECGTwaveEnds",2,5))
  end

  if type(S)<>1|~isreal(S) then
    error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),...
                   "ECGTwaveEnds",1))
  end
  if and(size(S)<>1)
    error(msprintf(_("%s: Wrong size for argument #%d: Vector expected.\n"),...
                   "ECGTwaveEnds",1))
  end
  
  if type(fs)<>1|~isreal(fs)|size(fs,"*")<>1 then
    error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),...
                   "ECGTwaveEnds",2))
  end
  if fs<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),...
                   "ECGTwaveEnds",2))
  end
  if type(Rpeaks)<>1|~isreal(Rpeaks)|or(round(Rpeaks)<>Rpeaks) then
    error(msprintf(_("%s: Wrong type for input argument #%d: Matrix of integers expected.\n"),...
                   "ECGTwaveEnds",3))
  end
  if and(size(Rpeaks)<>1)
    error(msprintf(_("%s: Wrong size for argument #%d: Vector expected.\n"),...
                   "ECGTwaveEnds",3))
  end
  if or(Rpeaks<=0) then
    error(msprintf(_("%s: Wrong values for input argument #%d: Elements must be positive.\n"),...
                   "ECGTwaveEnds",3))
  end
  //S = filtbyfft("bp",S-mean(S),fs, [0.5 250]);
  //Summation  window size (W)
  if nin<4 | isempty(swin) then
    swin = 0.128;//in seconds
  end
  swin=int(swin*fs);// in number of samples

  if nin<5 | isempty(mthrld) then
    mthrld = 6;
  end
  N=length(S);
  //length of  smoothing window p (used to compute \bar{s}_k  p<<W
  ptwin = ceil(0.016*fs); //0.016s

  nT = length(Rpeaks);
  if Rpeaks(nT)+200 > N then  nT = nT-1;end

  kTe = zeros(nT,1);
  Ttype=emptystr(nT,1);
  //length of the R-R intervals
  RR=[diff(Rpeaks) 0.8*fs];//0.8s
 
  //T-wave end search interval parameters: 
  //   The T wave peak and the T wave end for the kth beat are supposed to
  //   be in the interval [Leftbound(k) rightbound(k)]

  //   Compute boundaries for  R-R interval lengths greater than 0.88s
  al = 0;
  bl = 0.28*fs;//0.28s
  ar = 0.2;
  br = 0.404*fs;//0.404s
  Leftbounds = Rpeaks+floor(al*RR+bl);//ka
  Rightbounds = Rpeaks+ceil(ar*RR+br);//kb
  //    Compute boundaries for  R-R interval lengths less than 0.88s
  k=find(RR<0.88*fs);//0.88s
  if k<>[] then
    al = 0.15;
    bl = 0.148*fs;//0.148s
    ar = 0.7;
    br = -0.036*fs;//-0.036s
    Leftbounds(k) = Rpeaks(k)+floor(al*RR(k)+bl);//ka
    Rightbounds(k) = Rpeaks(k)+ceil(ar*RR(k)+br);//kb
  end
 
  //Insure  Rightbound being lower than the next peak index
  Rightbounds(1:$-1)=min(Rightbounds(1:$-1),Rpeaks(2:$));
  //Insure signal is long enough to be able to compute the indicator function
  Rightbounds($)=min(Rightbounds($),N-ptwin);
  Leftbounds($)=min(Leftbounds($),Rightbounds($)-2);
  if exists("%opt")==0 then %opt=[0 0];end
  if exists("%pause")==0 then %pause=[];end
  global ok;
  ok=%opt(2)==1;
  for knR=1:nT
    kR=Rpeaks(knR);
    leftbound=Leftbounds(knR)
    rightbound=Rightbounds(knR)
    // Compute the area indicator for all values of t
    Tval=conv(S(leftbound-swin+1:rightbound).^3,ones(1,swin),"valid")-...
         conv(S(leftbound-ptwin:rightbound+ptwin),ones(1,2*ptwin+1)*(swin/(2*ptwin+1)),"valid");
    if %opt(1)==0 then
      [kTe(knR),Ttype(knR)]=decide(Tval)
    else
      [kTe(knR),Ttype(knR)]=decide_n(Tval)
    end
  end
  clearglobal ok
endfunction




