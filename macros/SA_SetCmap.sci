function SA_SetCmap(d,fig)
  if argn(2)==1 then fig=gcf();end
  select convstr(d)
  case "jet" then  
    fig.color_map=jetcolormap(256);
  case "gray" then 
    cm=graycolormap(256)
    fig.color_map=cm($-10:-1:1,:); 
  end
endfunction
