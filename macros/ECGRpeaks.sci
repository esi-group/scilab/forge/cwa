// This file is part of the CWA toolbox
// Copyright (C) 2005 - INRIA - Qinghua Zhang
// Copyright (C) 2014 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function kRp = ECGRpeaks(S,fs)
//Detection of ECG R-peaks by parabolic fitting
//Authors
//  - Qinghua Zhang (original Matlab function) rpeak.m  part of the
//         ecgtwave package registered at APP 
//         under the number IDDN.FR.001.430041.000.S.P.2005.000.31230. 
//  - Serge Steer (translation to Scilab and vectorization)
  select argn(2)
  case 1 then
    if typeof(S)<>"sciecg" then
      error(msprintf(_("%s: Wrong type for argument %d: sciecg data structure expected.\n"),...
                     "ECGRpeaks",1))
    end
    if size(S,2)<>1 then
      error(msprintf(_("%s: Wrong size for argument %d: a single channel ecg expected.\n"),...
                     "ECGRpeaks",1))
    end
    fs=S.fs
    S=S.sigs
  case 2 then
    if type(S)<>1|~isreal(S) then
      error(msprintf(_("%s: Wrong type for argument %d: Real matrix expected.\n"),...
                     "ECGRpeaks",1))
    end
    if and(size(S)<>1)
      error(msprintf(_("%s: Wrong size for argument #%d: Vector expected.\n"),...
                     "ECGRpeaks",1))
    end
    S=S(:);
    if type(fs)<>1|~isreal(fs)|size(fs,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),...
                     "ECGRpeaks",2))
    end
    if fs<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),...
                     "ECGRpeaks",2))
    end
  end
    
 
 
  N=size(S,1);
  //S = filtbyfft("bp",S-mean(S),fs, [0.5 250]);
  fratio = fs/250; // Frequence ratio w.r.t. 250hz.

  // Indicator signal computation (ECG convolution with a parabolic function)
  //-----------------------------------------------------------------------
  hwin=ceil(5*fratio); // half window width for parabolic fitting
  weights=1:(-1/hwin):(1/hwin); // parabolic fitting weights in half window
  dsw = sqrt([weights($:-1:1), 1, weights])'; // square root of double side weights
  xw = [-[(-hwin:hwin).^2]', ones(2*hwin+1,1)];
  xw = dsw(:,[1 1]) .* xw;
  invxw = inv(xw'*xw)*xw';
  dsw=dsw';
  pth=[zeros(hwin,1);conv(S,invxw(1,:).*dsw,'valid').*conv(S,invxw(2,:).*dsw,'valid');zeros(hwin,1)]
    
  clear weights dsw xwinvxw
  
  // R peaks location
  //---------------
  srtlen = min(10000*fratio, N);
  sortedpth = gsort(pth(1:srtlen),'g','i');
  seuil = sortedpth(round(srtlen*0.975));
  //seuil = sortedpth(round(srtlen*0.98));
  if %t then //vectorized code
    //index of indicator signal values > seuil (neighbourhood of peaks)
    //indnz=find(pth>seuil);
    indnz=find(pth>=seuil);
    if pth(indnz(1)+1)<pth(indnz(1)) then
      //first detection correspond to a descending part of a R peak
      //ignore it
      indnz(1)=[];
    end
    indnz($+1)=N;
    //pth array splitting
    indsplit=[0 find(diff(indnz)>1)];
    //ith Peak is located between indnz(indsplit(i)+1) and indnz(indsplit(i+1)) 
    //RR_moy=mean(indnz(indsplit(2:$-1)+1)-indnz(indsplit(1:$-2)+1));
    //cnth=round(RR_moy/4); //make algo find too many R peaks with sel104(2)
    cnth = round(0.36*fs);
    //pc=conv(bool2s(pth>seuil),ones(1,cnth),'same');
    np=size(indsplit,'*')
    //select peak location in each pth segment
    kRp=zeros(1,np);
    count=0;
    i=1;
    //looking for the beginning of a peak (a value greater than the
    //threshold preceeded by at least cnth values lower than the
    //threshold 
    while i<np
      while i<np
        kd=indnz(indsplit(i)+1);
        if or(pth(max(1,kd-cnth):kd-1)>seuil) then 
          i=i+1;
        else
          break;
        end
      end
      //looking for the end of a peak (a value greater than the
      //threshold lollowed by at least cnth values lower than the threshold 
      while i<np
        kf=indnz(indsplit(i+1));
        if or(pth(kf+1:min(N,kf+cnth))>seuil) then 
          i=i+1;
        else
          // a peak lies between kd and kf
//          [dum, ind] = max(S(kd:kf));
          [dum, ind] = max(pth(kd:kf));
          count=count+1;
         
          kRp(count) = kd-1+ind;
          break;
        end
      end
      i=i+1;
    end
    kRp(count+1:$)=[];
  else //original code
    kRp=[];
    cnth = round(0.36*fs);//cnth=300
    debut = 1;
    fin = 0;
    for k=(1+cnth):N-cnth
      if pth(k)>seuil & ~or(pth((k-cnth):(k-1))>seuil) then
        debut = k;
      end
      if pth(k)>seuil & ~or(pth((k+1):(k+cnth))>seuil)
        fin = k;
        [dum, ind] = max(pth(debut:fin));
        ind = ind + debut - 1;
        kRp=[kRp,ind];
      end
    end
  end
  // Look for missed peaks
  //compute typicRR, the median value of the third shortest RR interval lengths
  srtlen = min(10000*fratio, size(kRp,'*'));
  RR = diff(kRp(1:srtlen));
  sortedRR = gsort(RR,'g','i');
  typicRR = median(sortedRR(1:ceil(srtlen/3)));

  //Treat possible beginning peaks
  beginexind = BeginPeakRecovery(kRp(1), pth, typicRR, seuil);

  if  ~isempty(beginexind) then
    kRp = [gsort(beginexind,'g','i'), kRp];
    //Following lines have to be redone since now kRp may have been changed.
    srtlen = min(10000*fratio, size(kRp,'*'));
    RR = diff(kRp(1:srtlen));
    sortedRR = gsort(RR,'g','i');
    typicRR = median(sortedRR(1:ceil(srtlen/3)));
  end

  indmiss = find(RR>typicRR*1.5);

  if ~isempty(indmiss)
    misscase = length(indmiss);
    vecextraind = [];
    for km=1:length(indmiss)
      vecextraind = [vecextraind, ...
                     PeakRecovery(kRp(indmiss(km)), kRp(indmiss(km)+1), pth, typicRR)];
    end
    keptind = find(pth(vecextraind)>0.3*seuil);
    kRp = gsort([kRp,  vecextraind(keptind)],'g','i');
  end

endfunction


//=================================================================
function exind = PeakRecovery(ind1, ind2, pth, typicnum)
// Recover missed peaks between two detected peaks

  hnum = ceil(0.5*typicnum);
  [dum, maxind] = max(pth((ind1+hnum):(ind2-hnum)));
  maxindglobal = maxind + ind1+hnum - 1;
  exind =  maxindglobal;

  if (maxindglobal-ind1)>typicnum*1.5 then
    exind = [PeakRecovery(ind1, maxindglobal, pth, typicnum),  exind];
  end

  if (ind2-maxindglobal)>typicnum*1.5 then
    exind = [exind,  PeakRecovery(maxindglobal, ind2, pth, typicnum)];
  end 
endfunction

//-----------------------------------
function exind = BeginPeakRecovery(ind1, pth0, typicnum, seuil)
// Recover missed peaks at the beginning of the signal

  if ind1>typicnum*0.7  then
    [maxv, maxi] = max(pth0(1:ceil(ind1-typicnum*0.3)));
    if maxv>seuil*0.6
      exind = [maxi BeginPeakRecovery(maxi, pth0, typicnum, seuil)];
    else
      exind = [];
    end
  else
    exind = [];
  end
endfunction
