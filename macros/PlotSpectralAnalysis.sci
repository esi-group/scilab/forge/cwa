// This file is part of the CardioVascular wave analysis toolbox
// Copyright (C) 2012 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function PlotSpectralAnalysis(res,InputNames,Title)
  if typeof(res)<>"RRA" then
    error(msprintf(_("%s: Wrong type for argument %d: %s expected.\n"),...
                   "PlotSpectralAnalysis",1,"RRA"))
  end
  fig=gcf();fig.axes_size = [860,600];
  idraw=fig.immediate_drawing;
  fig.immediate_drawing="off";
  opt=struct();
  t=res.time
  L=[];Phase_axes=[];
  if argn(2)<2 then 
    InputNames=[_("RR"),_("Vt")]
  end
  InputLab=msprintf(_("%s (ms)"),InputNames(1));
  if size(InputNames,'*')<2 then InputNames(2)=_("Vt");end
  AuxLab=InputNames(2)
  PhaseLab=msprintf(_("%s->%s Phase shift(rd)"),InputNames(1),InputNames(2));

  select size(res)
  case 12 then //CDM SPWVD
    if argn(2)<3 then  Title=_("CDM_SPWVD Cardio vascular analysis");end

    opt.title=Title;
    if size(res.Vt,'*')>1 then 
      A=MultiChannelPlot(t,res.RR,InputLab,...
                           res.Vt,AuxLab,...
                           [res.IFreq_CDM,res.IFreq_SPWVD],_("Frequency (Hz)"),...
                           [res.IAmp_CDM,res.IAmp_SPWVD,res.RRfiltered],_("Amplitude (ms)"),...
                            res.IPhase, PhaseLab,...
                            res.IDisp,_("Dispersion"),opt)
      
      L=[L legend(A(3),[_("CDM");_("SPWVD")])];
      
      A(4).data_bounds(1,2)=0;//only show positive part of the filtered input data
      A(4).children(1).foreground=color("gray");//RRfiltered
      L=[L legend(A(4),[_("CDM");_("SPWVD");_("RR filtered")])];

      Phase_axes=A(5);
      Phase_axes.user_data="rd";
   
    else 
      A=MultiChannelPlot(t,res.RR,InputLab,...
                           [res.IFreq_CDM,res.IFreq_SPWVD],_("Frequency (Hz)"),...
                           [res.IAmp_CDM,res.IAmp_SPWVD,res.RRfiltered],_("Amplitude (ms)"),...
                            res.IDisp,_("Dispersion"),opt)
      L=[L legend(A(2),[_("CDM");_("SPWVD")])];
      A(3).data_bounds(1,2)=0;//only show positive part of the filtered input data
      A(3).children(1).foreground=color("gray");//RRfiltered
      L=[L legend(A(3),[_("CDM");_("SPWVD");_("RR filtered")])];
    end
  case 8 then //CDM
    if argn(2)<3 then  Title=_("CDM Cardio vascular analysis");  end
    opt.title=Title;
    if  size(res.Vt,'*')>1 then 
      A=MultiChannelPlot(t,res.RR,InputLab,...
                           res.Vt,AuxLab,...
                           res.IFreq,_("Frequency (Hz)"),...
                           [res.IAmp,res.RRfiltered],_("Amplitude (ms)"),...
                            res.IPhase,PhaseLab,opt);
      
      A(4).children(1).foreground=color("gray");//RRfiltered
      A(4).data_bounds(1,2)=0; //only show positive part of the filtered input data
      L=[L legend(A(4),[_("Amplitude");_("RR filtered")])];

      Phase_axes=A(5);
      Phase_axes.user_data="rd";
    else
      A=MultiChannelPlot(t,res.RR,InputLab,...
                           res.IFreq,_("Frequency (Hz)"),...
                           [res.IAmp,res.RRfiltered],_("Amplitude (ms)"), ...
                         opt);
      A(3).children(1).foreground=color("gray");//RRfiltered
      L=[L legend(A(3),[_("Amplitude");_("RR filtered")])];
      A(3).data_bounds(1,2)=0;//only show positive part of the filtered input data
                               
    end
  case 9 then //SPWVD
    if argn(2)<3 then  Title=_("SPWVD Cardio vascular analysis"); end
    opt.title=Title;
    if  size(res.aux,'*')>1 then 
      A=MultiChannelPlot(t,res.RR,_("RR (ms)"),...
                         res.aux,_("Auxiliary"),...
                         res.IFreq,_("Frequency (Hz)"),...
                         [res.IAmp,res.RRfiltered],_("Amplitude (ms)"),...
                         res.IDisp,_("Dispersion"),opt)
      A(4).children(1).foreground=color("gray");//RRfiltered
      L=[L legend(A(4),[_("Amplitude");_("RR filtered")])];
      A(4).data_bounds(1,2)=0;//only show positive part of the filtered input data
    else
      A=MultiChannelPlot(t,res.RR,InputLab,...
                         res.IFreq,_("Frequency (Hz)"),...
                         [res.IAmp,res.RRfiltered],_("Amplitude (ms)"),...
                         res.IDisp,_("Dispersion"),opt)
      
      A(3).children(1).foreground=color("gray");//RRfiltered
      L=[L legend(A(3),[_("Amplitude");_("RR filtered")])]
      A(3).data_bounds(1,2)=0;//only show positive part of the filtered input data
    end
    
  end
  for k=1:size(A,'*')
    P=A(k).children(A(k).children.type=="Polyline");
    for i=1:size(P,'*')
      datatipSetDisplay(P(i),TipDisplay);
      datatipSetInterp(P(i),%t);
    end
  end
  ud=fig.user_data;
  ud.LegendHandle=L;
  ud.PhaseHandle=Phase_axes;
  set(fig,"user_data",ud);

  submenus=[_("Hide legends"); _("Show legends"); _("Phase in degree"); _("Phase in rd")];
  delmenu(fig.figure_id,_("&Options"))
  addmenu(fig.figure_id,_("&Options"),submenus,list(2,"PSAOptionsMenu"))
  fig.immediate_drawing = idraw;
 
endfunction
function str=TipDisplay(curve, pt, index)
  str=msprintf('t=%g\ny=%-0.2g',pt(1),pt(2))
endfunction
