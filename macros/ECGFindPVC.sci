//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function kesv=ECGFindPVC(kRp)
//Look for venticular extra systole given the sequence of R peak indexes.
//
//kRp : R peaks indexes in the ECG signal
//kesv: indexes of R peaks at the origin of a venticular extra systole.
//
//Let  A B C D denote the length of four consecutive RR intervals the
// agorithms detects a venticular extra systole if B<<A & B+C~A+D

  RpRp=diff(kRp);
  A=RpRp(1:$-3);
  B=RpRp(2:$-2);
  C=RpRp(3:$-1);
  D=RpRp(4:$);

  kesv=find((B<A*0.8)& abs(((B+C)-(A+D)))./A<0.1);
  if kesv<>[] then kesv=kesv+2;end
  //kesv=kRp(kesv)
 // kesv=find(diff(RpRp)>180)+1;//index des pics correspondant a des extrasystoles
  //kesv=[];//pour claire

endfunction
