//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ecg=ECGFilter(ecg,ftype,forder,cfreq,wtype,fpar)
  fs=ecg.fs
  if argn(2)==1 then
    exprs=["""hp""";
           """hm"""
           "115";
           string(fs);
           string(0.05*fs)
           string(0.45*fs);
           "0.5"]
    [ok,values,exprs]=wfir_gui(exprs)
    if ~ok then return,end
    ftype=values.ftype
    forder=values.forder
    cfreq=[values.low values.high]
    wtype=values.wtype
    fpar=values.fpar
  end
  F=wfir(ftype,forder,cfreq,wtype,fpar)
  nvoies=size(ecg,2);
  for k=1:nvoies
    ecg.sigs(:,k)=conv(ecg.sigs(:,k),F,"same");
  end
endfunction
