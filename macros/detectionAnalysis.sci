//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [d,M,S]=detectionAnalysis(d1,d2,nclass)
//Correlation de la seconde voie par rapport à la premiere
//d1 premiere voie
//d2 seconde voie  
//nclass: nombre de classe pour l'histogramme selon d1
  if argn(2)<3 then nclass=20;end
  mn1=min(d1);mx1=max(d1);
  mn2=min(d2);mx2=max(d2);
 //calcul de l'histogramme par rapport à la premiere variable
  d=linspace(mn1,mx1,nclass);pas=d(2)-d(1) //creation des classes
  [ind1,occ1]=dsearch(d1,d);//creation de l'histogramme

  M=zeros(1,nclass-1)
  S=zeros(1,nclass-1)
  //parcours des données de chacune des  classes 
  ieee_save= ieee();
  ieee(2);
  for k=1:nclass-1
    dk=d2(find(ind1==k)); //valeurs de d2 dans la classe courante de d1
    M(k)=mean(dk);
    S(k)=stdev(dk);
  end 
  ieee(ieee_save);
  d=d(2:$)-pas/2//gestion des piquets et des intervalles
endfunction
