//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function data=SPWVD_Analysis(RR,Vt,freq_sampling,BPfrequency_bounds,BPfilterlength,options)
 
  err_msg1=_("%s: Wrong type for input argument #%d: A real vector of float expected.\n");
  err_msg2=_("%s: Wrong type for input argument #%d: A real positive number expected.\n");
  fname="SPWVD_Analysis";
  if type(RR)<>1|~isreal(RR)|and(size(RR)>1) then 
    error(msprintf(err_msg1,fname,1))
  end
  if type(Vt)<>1|~isreal(Vt)|and(size(Vt)>1) then 
    error(msprintf(err_msg1,fname,2))
  end
  if size(Vt,'*')>1  then
    if size(Vt,'*')<>size(RR,'*') then
      err_msg=_("%s: Incompatible length for input arguments #%d and #%d.\n")
      error(msprintf(err_msg,fname,1,2))
    end
    Vt=matrix(Vt,1,-1);
    withVt=%t
    nplot=4;
  else
    withVt=%f
    nplot=3;
  end
  if type(freq_sampling)<>1|~isreal(freq_sampling)|size(freq_sampling,'*')<>1|freq_sampling<=0 then 
    error(msprintf(err_msg2,fname,3))
  end

  if type(BPfrequency_bounds)<>1|~isreal(BPfrequency_bounds)| size(BPfrequency_bounds,'*')<>2 then 
    err_msg=_("%s: Wrong type for input argument #%d: A real vector expected.\n");
    error(msprintf(err_msg,fname,4))
  end
  if size(BPfrequency_bounds,'*')<>2 then 
    err_msg=_("%s: Wrong size for input argument %s: An array of size %d expected.\n");
    error(msprintf(err_msg,fname,4))
  end
  if or(BPfrequency_bounds<=0)|BPfrequency_bounds(1)>=BPfrequency_bounds(2) then 
    err_msg=_("%s: Wrong value for input argument %s: A positive increasing array expected.\n");
    error(msprintf(err_msg,fname,4))
  end
  if  or(frequency_bounds(2)>0.5*freq_sampling) then 
    err_msg=_("%s: Wrong values for input argument #%d: The elements must be less than Nyquist frequency %f.\n");
    error(msprintf(err_msg,fname,4,0.5*freq_sampling))
  end

  if type(BPfilterlength)<>1|~isreal(BPfilterlength)|size(BPfilterlength,'*')<>1|BPfilterlength<=0 then 
    error(msprintf(err_msg2,fname,5))
  end
  if modulo(size(BPfilterlength,'*'),2)<>1 then
    err_msg=_("%s: Wrong value for  input argument #%d: An odd number expected.\n")
    error(msprintf(err_msg,fname,5))
  end
  if argn(2)==6 then
    err_msg2=_("%s: Wrong type for input argument #%d: A struct with fields in {%s} expected.\n")
    err_msg3=_("%s: Wrong type for input argument #%d: A real positive number expected.\n");
    err_msg4=_("%s: Wrong value for field %s of input argument #%d: An odd number expected.\n")
    err_msg5=_("%s: Wrong value for field %s of input argument #%d: A power of two expected.\n")
    err_msg6=_("%s: Wrong value for input argument #%d: A struct with fields %s expected.\n")

    if typeof(options)=="st" then
      legal_fields=["lowpass","timewindowlength","frequencywindowlength","frequencybins"];
      fn=fieldnames(options)
      for k=1:size(fn,'*')
        select fn(k)
        case "lowpass" then
          //low pass filter
          LP=options.lowpass
          if type(LP)<>1|~isreal(LP)|and(size(LP)>1) then 
            error(msprintf(err_msg2,fname,fn(k),6))
          end
          if modulo(size(LP,'*'),2)<>1 then
            error(msprintf(err_msg3,fname,fn(k),6))
          end
           LP=LP/sum(LP);//normalize to get a unit gain at 0 frequency 
           LPwl=size(LP,'*')
        case "timewindowlength" then
          //time smoothing window length
          twl=options(fn(k));
          if type(twl)<>1|~isreal(twl)|size(twl,'*')<>1|twl<1 then 
            error(msprintf(err_msg3,fname,fn(k),6))
          end
          if modulo(size(twl,'*'),2)<>1 then
            error(msprintf(err_msg4,fname,fn(k),6))
          end
        case "frequencywindowlength"  then
          //frequency smoothing window length
          fwl=options(fn(k));
          if type(fwl)<>1|~isreal(fwl)|size(fwl,'*')<>1|fwl<1 then 
            error(msprintf(err_msg3,fname,fn(k),6))
          end
          if modulo(size(fwl,'*'),2)<>1 then
            error(msprintf(err_msg4,fname,fn(k),6))
          end
        case "frequencybins"  then
          //frequency smoothing window length
          nf=options(fn(k));
          if type(nf)<>1|~isreal(nf)|size(nf,'*')<>1|nf<1 then 
            error(msprintf(err_msg3,fname,fn(k),6))
          end
          if nf<>2^round(log2(nf)) then
            error(msprintf(err_msg5,fname,fn(k),6))
          end
        else
          error(msprintf(err_msg6,fname,6,strcat(legal_fields,",")))
        end
      end
    else
      error(msprintf(err_msg6,fname,6,strcat(legal_fields,",")))
    end
  end
  //narrow band filter
  BPdelay=(BPfilterlength-1)/2;
  BP=wfir("bp",BPfilterlength,BPfrequency_bounds/freq_sampling,"hm",[0  0]);

  
  RR=matrix(RR,1,-1)
  sig=filter(BP,1,[RR RR($)*ones(1,BPdelay)]);

  // Call the SPWVD demodulation
  if argn(2)==6 then
    [T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig,options);
  else
    [T,IFreq,IAmp,IPow,IDisp,delay]=TimeMoments(sig); 
  end

  // Take sampling frequency into account
  IFreq=IFreq*freq_sampling;

  t=(0:size(IFreq,'*')-1)/freq_sampling;

  d1=BPdelay/freq_sampling;


  fn=["RRA","time","RR","aux","RRfiltered","IFreq","IAmp","IPow","IDisp"]

   if  withVt then
     data=tlist(fn,...
                t(BPdelay+delay+1:$)',...
                RR',...
                Vt',...
                sig(BPdelay+1:$)',...
                IFreq(BPdelay+delay+1:$)',...
                IAmp(BPdelay+delay+1:$)',...                
                IPow(BPdelay+delay+1:$)',...
                IDisp(BPdelay+delay+1:$)');
  else
     data=tlist(fn,...
                t(BPdelay+delay+1:$)',...
                RR',...
                [],...
                sig(BPdelay+1:$)',...
                IFreq(BPdelay+delay+1:$)',...
                IAmp(BPdelay+delay+1:$)',...  
                IPow(BPdelay+delay+1:$)',...
                IDisp(BPdelay+delay+1:$)');
  end

endfunction
