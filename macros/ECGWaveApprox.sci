//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [Ts,G,Err]=ECGWaveApprox(S,Pos)
  if argn(2)<2 then
    //    P    Q    R   S    T
    Pos=[0.25;0.47;0.5;0.55;0.85;0.35;0.9;0.5];
  end
  Err=[]
  kRp=ECGRpeaks(S);
  fs=S.fs
  S=S.sigs;
  Scale=1/max(abs(S(kRp)))
  S=S*Scale;
  T=(0:size(S,'*')-1)/fs;

  ng=size(Pos,'*'); //number of gaussian
  Gd=Pos(:)*[1 0.1 0.1 0.1 0 0];

  mg=size(Gd,2); //number of parameters per gaussian
  //initialization of result arrays
  G=zeros(ng,mg,size(kRp,'*')-2) //parameters of fitting gaussians
  Ts=zeros(1,size(kRp,'*')-2) //split instants

  l=round((kRp(2)-kRp(1))/2);
  kf=max(1,kRp(1)-l)-1;
  epsx=0.1*ones(ng,mg);epsx(3,2)=1;epsx(:,1)=Gd(:,1);epsx(:,4)=1;
  bsup=ones(Gd);
  binf=-ones(Gd);binf(:,[1,3 4])=1e-8
  for i=1:size(kRp,'*')
    //extract the wave which contains the peak number i
    kd=kf+1;
    if i==size(kRp,'*') then
      l=round((kRp(i)-kRp(i-1))/2);
      kf=min(kRp(i)+l,size(S,'*'));
    else
      l=round((kRp(i+1)-kRp(i))/2);
      kf=kRp(i)+l;
    end
    s=S(kd:kf)';
    t=linspace(0,1,kf-kd+1)
    //Set initial point 
    [m,k]=max(abs(s));m=s(k);
    count=0;fm=%inf
    while %t
      Gd(1:5,1)=t(k)+[-0.25; //approximate position for the P peak
                      -0.015;//approximate position for the Q peak
                      0;     //approximate position for the R peak
                      0.02;  //approximate position for the S peak
                      0.25]; //approximate position for the T peak
      Gd(3,2)=m; //approximate amplitude value for the Rpeak

      //look for optimal solution
      [fopt, Gopt,gopt, work, iters, evals, flag] = optim(list(ECGWaveOptim,ng,t,s),Gd,'ar',1500,500,1e-10,0,epsx);
      
      if fopt<0.05 then 
        Gd=Gopt;epsx(:,1)=Gd(:,1);break
      else 
        if i<3 then break;end
        //mprintf("i=%d,count=%d,fopt=%.2f,fm=%.2f\n",i,count,fopt,fm)
        Gd=G(:,:,i-2-count)
        if fopt<fm then
          fm=fopt
          Gm=Gopt
        end
        if count>2|i-2-count<2 then
          fopt=fm
          Gopt=Gm
          break
        end
        count=count+1
      end
    end

    Gopt(:,3:4)=abs(Gopt(:,3:4));//width appears squared
                                                                                                   
    //memorize the wave parameters
    G(:,:,i)=Gopt
    Ts(i)=T(kd);

    //Display results
    
    //ECGWaveGenShow(t,Gopt);xpoly(t,s);e=gce();e.thickness=3;
    sr=ECGWaveGen(t,Gopt);
    clf;plot(t,s,'b',t,sr,'r');
    for kk=1:size(Gopt,1)
      [m,ii]=min(abs(t-Gopt(kk,1)))
      plot(t(ii),sr(ii),'+g')
    end
    //mprintf("i=%d,flag=%d,evals=%d,fopt=%.2g,||gopt||=%.2g\n",i,flag,evals,fopt,norm(gopt))
    Err=[Err fopt];
  end
  Ts($+1)=T(kf);
  G(:,[2 5 6],:)=G(:,[2 5 6],:)/Scale
endfunction

function [f,g,ind]=ECGWaveOptim(x,ind,ng,t,sr)
  alpha_p=0.5;//penalization factor
  alpha_b=0.00;
  with_grad=or(ind==[3 4])
  ng=size(x,1); //the number of gaussians
  pos=x(:,1);
  //if or(pos<=1e-5|pos>=1-1e-5) then ind=-1,f=0,g=zeros(x);return;end
  //penalisation on PQRST positions 
  pen_p=[pos(1)-0.25;//P
       pos(2)-0.45;//Q
       pos(3)-0.5; //R
       pos(4)-0.54;//S
       pos(5)-0.9];//T
  pen_b=[x(:,5)-x(:,6)]
  if with_grad then
    [s,dsdx]=ECGWaveGen(t,x);
  else
    s=ECGWaveGen(t,x);
  end
 
  e=s-sr;
  f=e*e'+alpha_p*(pen_p'*pen_p)+alpha_b*(pen_b'*pen_b)
  if with_grad then
    g=2*matrix(matrix(dsdx,ng*6,-1)*e',ng,6);
    g(1:5,1)=g(1:5,1)+2*alpha_p*pen_p; //adding penalization effect on PQRST positions
    g(:,5)=g(:,5)+2*alpha_b*x(:,5);
    g(:,6)=g(:,6)-2*alpha_b*x(:,6);
  end
endfunction

