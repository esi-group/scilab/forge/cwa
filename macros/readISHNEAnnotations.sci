// This file is part of the e Cardiovascular Wawes Analysis toolbox
// Copyright (C)  - University of Rochester Medical Center (URMC). 
// Copyright (C) 2013 - INRIA - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [Ann, Loc,head]=readISHNEAnnotations(fileName, startPos,endPos);
// Read ISHNE binary ann file : 
// Input--------------------------------------------------------       
//  fileName : the ishne binary annotation filename including the path
//  startPos   : the value (in number of sample) of the first requested informations
//  endPos   : the value (in number of sample) of the last requested informations
// Ouput--------------------------------------------------------
//  head : ishne header info
//  Ann         : annotation of the beats
//  RR          : R peak to peak intervals (in number of sample)
//
//This file is derived from the Matlab function found at 
//  http://thew-project.org/code_library.htm
//http://thew-project.org/papers/ishneAnn.pdf
  if argn(2)<2 then 
    startPos=0;
  elseif type(startPos)<>1|size(startPos,'*')<>1|startPos<0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer value expected.\n"),"readISHNEAnnotations",2))
  end
 
  if argn(2)<3 then 
    endPos=%inf;
  elseif type(endPos)<>1|size(endPos,'*')<>1|endPos<0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer value expected.\n"),"readISHNEAnnotations",3))
  end
  
  fid=mopen(fileName,'rb');
  magicNumber = ascii(mget(8,"uc",fid));
  if magicNumber<>"ANN  1.0" then
    mclose(fid)
    error("The given file is not a ISHNE annotation file")
  end

  head=readISHNEHeader(fid);
 
  // read first annotation location
  loc = mget(1,"i",fid);
  
  // get number of beats
  currentPosition = mtell(fid);
  mseek(0,fid, "end");
  endPosition = mtell(fid);
  numBeat = (endPosition-currentPosition)/4;
  mseek(currentPosition,fid,"set")
  
  //skip first beats if required
  for i = 1:numBeat
    if loc < startPos then
      mget(2,"uc",fid);
      loc=loc+mget( 1,"us",fid)
    else
      numBeat=numBeat+1-i
      break
    end
  end
 
  Ann=emptystr(numBeat,1);
  Loc=zeros(numBeat+1,1);
  Loc(1)=loc;
  for i = 1:numBeat
    Ann(i) = ascii(mget(1,"c",fid));
    internalUse = mget(1,"c",fid);
    loc = mget( 1,"s",fid) + loc;
    Loc(i+1)=loc;
    if (loc > endPos) then
      //skip last beats if required
      Ann=Ann(1:i)
      Loc=Loc(1:i+1)
      break;
    end
  end
  mclose(fid);
  Loc=Loc+1
endfunction
