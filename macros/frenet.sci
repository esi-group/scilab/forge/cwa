//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function [t,n,b,g,tau]=frenet(x,y,z)
//http://fr.wikipedia.org/wiki/Rep%C3%A8re_de_Frenet
  select argn(2)
  case 1 then  //x,y,z are the columns of the input args
    p=x
  case 3 then 
    N=size(x,'*');
    p=[matrix(x,-1,1) matrix(y,-1,1) matrix(y,-1,1)];
  end
  ieees=ieee();ieee(2);
  [N,M]=size(p,1)
  //calcul de l'abscisse curviligne
  s=[0;cumsum(sqrt(sum(diff(p,1,1).^2,2)))]
 
  t=diff(p,1,1)./diff(s);
  t1=sqrt(sum(t.^2,2));
  t=t./(t1*ones(1,3));
  t($+1,:)=t($,:);
 
  n=diff(t,1,1)./diff(s)
  g=sqrt(sum(n.^2,2));
  
  
  n=n./(g*ones(1,3));
  n($,:)= n($-1,:);
  n($+1,:)= n($,:);
  g($,:)= g($-1,:);
  g($+1,:)= g($,:);

  
  //cross product T^N
  b=[t(:,2).*n(:,3)-t(:,3).*n(:,2)
     t(:,3).*n(:,1)-t(:,1).*n(:,3)
     t(:,1).*n(:,2)-t(:,2).*n(:,1)];
  b=b./(sqrt(sum(b.^2,2))*ones(1,3));
  db=diff(b,1,1)./diff(s);
  tau=-sqrt(sum(db.^2,2))
  ieee(ieees)
 endfunction
