function SA_draw(fig)
  scf(fig)
  fig_ud=fig.user_data
  SP=fig_ud.SP
  fig.immediate_drawing = "off"
  ax=gca();
  C=ax.children
  zb=ax.zoom_box
  select fig_ud.draw_type
  case "PseudoColor" then
    if size(C,'*')>0 then
      if C.type=="Grayplot" then
        data=tlist(["grayplotdata";"x";"y";"z"], fig_ud.T(:),fig_ud.F(:),SP)
        C.data=data
      else
        delete(C)
        grayplot(fig_ud.T,fig_ud.F,SP);
      end
    else
      grayplot(fig_ud.T,fig_ud.F,SP);
    end
    ax.view="2d"
    ax.cube_scaling="off";
  case "3D" then
    if size(C,'*')>0 then
      if C.type=="Plot3d" then
        data=tlist(["grayplotdata";"x";"y";"z"], fig_ud.T,fig_ud.F,SP)
        C.data=data
        ax.data_bounds(:,3)=[min(SP);max(SP)]
      else
        delete(C)
        plot3d1(fig_ud.T,fig_ud.F,SP);C=gce();
       end
    else
       pause
      plot3d1(fig_ud.T,fig_ud.F,SP);C=gce();
     end
    C.color_mode = -1;
    C.color_flag = 1;
    C.foreground = -1;

    if zb<>[] then
      zb(5:6)=[min(SP) max(SP)]
      ax.zoom_box=zb;
    end
    ax.cube_scaling="on";
    ax.view="3d"
  end
  fig.immediate_drawing = "on"
endfunction
