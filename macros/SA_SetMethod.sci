function SA_SetMethod(m,fig)
  if argn(2)==1 then fig=gcf();end
  fig_ud=fig.user_data
  fig_ud.method=m
  set(fig,"user_data",fig_ud)
  SA_compute(fig)
  SA_draw(fig)
endfunction
