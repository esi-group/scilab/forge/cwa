//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function ECGShowDetections(S,loc)
   if typeof(S)<>"sciecg" then
    error(msprintf(_("%s: Wrong type for argument %d: sciecg data structure expected.\n"),...
                                      "ECGShowDetections",1))
  end
  if size(S,2)<>1 then
     error(msprintf(_("%s: Wrong size for argument %d: a single channel ecg expected.\n"),...
                                      "ECGShowDetections",1))
  end
   if size(loc,1)<>9 then
    error(msprintf(_("%s: Wrong size for input argument #%d: row dimension must be equal to %d.\n"),...
                   "ECGShowDetections",2,9))
  end

  
  
  M=["Rp","Re","To","Tp","Te","Po","Pp","Pe","Qo"];

  drawlater()
  t=(0:size(S,'*')-1)'/S.fs
  plot(t,S.sigs)
  xlabel(_("Time (s)"))
  ylabel(_("mV"))
  set(gca(),"grid",color("lightgray")*ones(1,2))
  E=[];
  L=[];
  for k=1:9
    K=loc(k,:)';
    K=K(K>0);
    if K<>[] then
      xpoly(t(K),S.sigs(K),"marks");
      E=[E,gce()];
      L=[L,M(k)];
      ECGSetMarks(E($),k)
    end
  end
  legend(E,L)
  drawnow()
endfunction
