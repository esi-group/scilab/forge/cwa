function outliers=sdFilter(s,sdLimit)
//sdFilter: Locate outliers based on standard deviation
  outliers = abs(s-mean(s)) > sdLimit*stdev(s);
endfunction
