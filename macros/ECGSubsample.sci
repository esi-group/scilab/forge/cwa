//This file is part of the Cardiovascular Wawes Analysis toolbox
//Copyright (C) 2014 - INRIA - Serge Steer
//This file must be used under the terms of the CeCILL.
//This source file is licensed as described in the file COPYING, which
//you should have received as part of this distribution.  The terms
//are also available at
//http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function S=ECGSubsample(S,f_ratio,nf)
  if argn(2)<3 then nf=64;end
  if typeof(S)<>"sciecg" then
    error(msprintf(_("%s: Wrong type for argument %d: sciecg data structure expected.\n"),...
                                      "ECGSubsample",1))
  end

  if type(f_ratio)<>1|~isreal(f_ratio)|size(f_ratio,"*")<>1 then
    error(msprintf(_("%s: Wrong type for argument %d: Real scalar expected.\n"),...
                   "ECGSubsample",2))
  end
  if int(f_ratio)<>f_ratio then
    error(msprintf(_("%s incompatible arguments %d and %d: arg%d must be a multiple of arg%d\n"),...
                   "ECGSubsample",2,3,2,3))
  end
  if f_ratio<2  then
    error(msprintf(_("%s wrong value for argument %d: must be greater or equal to %f\n"),...
                   "ECGSubsample",2,2))
  end
  fs=S.fs
  S=S.sigs
  //Anti aliasing filter cuts frequency components whose frequency is greater than fe_out
  F_lp=wfir("lp",nf,[0.5/f_ratio,0],'hn',[0 0]);
  //F_lp is a linear phase filter. It introduces a group delay equal to
  //nf/2
  //subsampling
  for k=1:size(S,2);
    Sf=filter(F_lp,1,[S(nf:-1:2,k);S(:,k)]')';
    S(:,k)=Sf(nf:$);
  end
 
  S=sciecg(fs/f_ratio,S(1:f_ratio:$,:))
endfunction
