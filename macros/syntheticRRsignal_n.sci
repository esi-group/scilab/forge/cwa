function [t,RR]=syntheticRRsignal_n(varargin)
//short term RR representative synthetic signal
//uses fmsin  of th  stftb toolbox
// t signal time instant in seconds
// RR signal in ms
//default values
  std=0.5;//4
  ampl_VLF=[0.5 0.6]; //ms [0.8 0.9]
  f_VLF=[0.005 0.03];//hz
  RRmean=1000; //ms
  fs=4;//hz
  tmax=600;//s
  RRmean=1000; //ms
  f_LF=[0.09-0.01 0.09+0.01 0.027];
  ampl_LF=4
  f_HF=[0.25-0.01 0.25+0.01 0.05];
  ks=floor(fs*tmax*[0.467 0.508 0.567 0.617 1]);
  ampl_HF=[ones(1,ks(1)-1),...
           linspace(1,0,ks(2)-ks(1)+1),...
           zeros(1,ks(3)-ks(2)-1),...
           linspace(0,10,ks(4)-ks(3)+1),...
           10*ones(1,ks(5)-ks(4)+1)];
  
  nv=size(varargin)
  if modulo(nv,2)<>0 then
    error(msprintf(_("%s: Wrong number of input argument(s): An even number expected.\n"),"syntheticRRsignal"))
  end
  keys=["fs","tmax","RRmean","std","f_VLF","ampl_VLF","f_LF","ampl_LF","f_HF","ampl_HF"];
  defkeys=[]
  for k=1:2:nv,
    key=varargin(k);
    if and(key<>keys) then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),"syntheticRRsignal",k,strcat(keys,",")))
    end
    defkeys=[defkeys key];
  end
  for k=1:2:nv
    key=varargin(k);
    v=varargin(k+1);
    select key
    case "fs"
      if type(v)<>1|size(v,'*')<>1|~isreal(v)|v<=0 then
        error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),"syntheticRRsignal",k+1))
      end
      fs=v
    case "tmax"
      if type(v)<>1|size(v,'*')<>1|~isreal(v)|v<=0 then
        error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),"syntheticRRsignal",k+1))
      end
      tmax=v
      if and("ampl_HF"<>defkeys) then
        ks=floor(fs*tmax*[0.467 0.508 0.567 0.617 1]);
        ampl_HF=[ones(1,ks(1)-1),...
                 linspace(1,0,ks(2)-ks(1)+1),...
                 zeros(1,ks(3)-ks(2)-1),...
                 linspace(0,10,ks(4)-ks(3)+1),...
                 10*ones(1,ks(5)-ks(4)+1)];
      end
    case "RRmean"
      if type(v)<>1|size(v,'*')<>1|~isreal(v)|v<=0 then
        error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),"syntheticRRsignal",k+1))
      end
      RRmean=v
    case "std"
      if type(v)<>1|size(v,'*')<>1|~isreal(v)|v<=0 then
        error(msprintf(_("%s: Wrong value for input argument #%d: Must be a positive scalar.\n"),"syntheticRRsignal",k+1))
      end
      std=v
    case "f_VLF"
      if type(v)<>1|~isreal(v)|or(v<=0) then
        error(msprintf(_("%s: Wrong value for input argument #%d: A positive real array expected.\n"),"syntheticRRsignal",k+1))
      end
      if size(v,'*')<>2 then
        error(msprintf(_("%s: Wrong size for input argument #%d: A %d elements array expected.\n"),"syntheticRRsignal",k+1))
      end
      f_VLF=v
    case "ampl_VLF"
      if type(v)<>1|~isreal(v)|or(v<=0) then
        error(msprintf(_("%s: Wrong value for input argument #%d: A positive real array expected.\n"),"syntheticRRsignal",k+1))
      end
      if size(v,'*')<>2 then
        error(msprintf(_("%s: Wrong size for input argument #%d: A %d elements array expected.\n"),"syntheticRRsignal",k+1))
      end
      ampl_VLF=v
    case "f_LF"
      if type(v)<>1|~isreal(v)|or(v<=0) then
        error(msprintf(_("%s: Wrong value for input argument #%d: A positive real array expected.\n"),"syntheticRRsignal",k+1))
      end
      if size(v,'*')<>3 then
        error(msprintf(_("%s: Wrong size for input argument #%d: A %d elements array expected.\n"),"syntheticRRsignal",k+1))
      end
      f_LF=v
    case "ampl_LF"
      if type(v)<>1|~isreal(v)|or(v<=0) then
        error(msprintf(_("%s: Wrong value for input argument #%d: A positive real array expected.\n"),"syntheticRRsignal",k+1))
      end
      ampl_LF=matrix(v,1,-1)
    case "f_HF"
      if type(v)<>1|~isreal(v)|or(v<=0) then
        error(msprintf(_("%s: Wrong value for input argument #%d: A positive real array expected.\n"),"syntheticRRsignal",k+1))
      end
      if size(v,'*')<>3 then
        error(msprintf(_("%s: Wrong size for input argument #%d: A %d elements array expected.\n"),"syntheticRRsignal",k+1))
      end
      f_HF=v
    case "ampl_HF"
      if type(v)<>1|~isreal(v)|or(v<=0) then
        error(msprintf(_("%s: Wrong value for input argument #%d: A positive real array expected.\n"),"syntheticRRsignal",k+1))
      end
      ampl_HF=matrix(v,1,:)
    end
  end
  t=0:1/fs:tmax; //4hz sampling
  N=size(t,'*')
  if and(size(ampl_LF,"*")<>[1 N]) then
    error(msprintf(_("%s: Wrong size for input argument #%d: %d or %d elements expected.\n"),"syntheticRRsignal",2*(find(keys=="ampl_LF")-1)+1,1,N))
  end
  
  if and(size(ampl_HF,"*")<>[1 N]) then
    pause
    error(msprintf(_("%s: Wrong size for input argument #%d: %d or %d elements expected.\n"),"syntheticRRsignal",2*(find(keys=="ampl_HF")-1)+1,1,N))
  end
  //Very low frequency band signal (a 2 components signal)

  VLF=ampl_VLF(1)*sin(2*%pi*f_VLF(1)*t)+ampl_VLF(2)*sin(2*%pi*f_VLF(2)*t)
  
  //Low frequency band signal 
  f_LF=f_LF/fs; //normalized frequencies
  
  // frequency modulation
  [LF,if_LF]=fmsin(N,f_LF(1),f_LF(2),round(1/f_LF(3)),1,(f_LF(1)+f_LF(2))/2);
  // amplitude modulation
  LF=ampl_LF.*real(LF)';
  
  //High frequency band signal
  f_HF=f_HF/fs; //normalized frequencies
  
  // frequency modulation
  [HF,if_HF]=fmsin(N,f_HF(1),f_HF(2),round(1/f_HF(3)),1,(f_HF(1)+f_HF(2))/2);
  // amplitude modulation
  HF=ampl_HF.*real(HF)'
  //White gaussian additive noise
  WGN= grand(1,N,"nor",0,std);
  
  RR=RRmean+VLF+LF+HF+WGN
endfunction
