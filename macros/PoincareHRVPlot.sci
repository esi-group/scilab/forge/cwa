function PoincareHRVPlot(RR,varargin)
//SD1 : standard deviation of points perpendicular to the axis of the
//      line of identity
//SD2:  standard deviation  of points along the axis of the
//      line of identity 
  density=%f;
  ellipse=%f;
  for v=varargin
    select v
    case "ellipse" then 
      ellipse=%t;
    case "density" then
      density=%t;
    case "points" then
      density=%f;
    end
  end
  //clf;
  drawlater()
  //f=gcf();
  ax=gca();ax.isoview="on";
  RR=RR(:);

  if density then
    N=ceil(sqrt(size(RR,'*')))
    dRR=linspace(min(RR),max(RR),N)
    occ=dsearch2D([RR(1:$-1) RR(2:$)],dRR,dRR)
    dRR=dRR(1:$-1)+(dRR(2)-dRR(1))/2
    mnx=min(dRR);mxx=max(dRR);mny=mnx,mxy=mny
    f=gcf();
    f.color_map=jetcolormap(max(occ));
    //f.color_map(1,:)=[0 0 0];
    grayplot(dRR,dRR,occ)
    ax.tight_limits="on";
    cb=ax.data_bounds
  else
    mnx=min(RR(1:$-1));mxx=max(RR(1:$-1));
    mny=min(RR(2:$));mxy=max(RR(2:$));
    dx=(mxx-mnx)/20
    dy=(mxy-mny)/20
    mnx=mnx-dx;mxx=mxx+dx;
    mny=mny-dy;mxy=mxy+dy;
    plot(RR(1:$-1),RR(2:$),'ob');
    e=gce();e.children.mark_size=3;
    cb=[mnx mny;mxx mxy]
    ax.data_bounds=cb;
  end
  
  if ellipse then
    m1=mean(RR(1:$-1));
    m2=mean(RR(2:$));
    
    sd=0.5*stdev(diff(RR))^2
    SD1=sqrt(sd)
    SD2=sqrt(2*stdev(RR)^2-sd);
    a=-%pi/4
    R=[cos(a) sin(a);-sin(a) cos(a)]
    t=linspace(0,2*%pi,200);
    sc=R*[SD2*cos(t);SD1*sin(t)];
    xpoly(m1+sc(1,:),m2+sc(2,:))
    
    e=gce();e.thickness=2;e.foreground=color('red');
    b=m2-m1
    if mnx>=mny then p1=[mnx,mnx+b];else p1=[mny-b, mny];end
    if mxx>=mxy then p2=[mxx,mxx+b];else p2=[mxy-b,mxx];end
    xpoly([p1(1) p2(1)],[p1(2) p2(2)]);e=gce();

    e.line_style=3;e.thickness=2;
    b=m2+m1
    if b-mnx<=mxy then p1=[b-mxy,mxy];else p1=[mnx, b-mnx];end
    if b-mxx<=mny then p2=[b-mny,mny];else p2=[mxx, b-mxx];end
    xpoly([p1(1) p2(1)],[p1(2) p2(2)]);e=gce();
    e.line_style=3;e.thickness=2
    ax.x_label.font_size=3;ax.x_label.text="$RR_n$";
    ax.y_label.font_size=3;ax.y_label.text="$RR_{n+1}$"
  end
  ax.data_bounds=cb
 
  drawnow()
endfunction
