function SA_ControlMenu(k,win)
  fig = get_figure_handle(win)
  C=fig.children
  c=C(C.type=="uicontrol")//the control panel
  g=C(C.type=="Axes")//the graph
  select k
  case 1 then //hide
    c.children.visible="off"
    c.visible="off"
    g.axes_bounds=[0 0 1 1]
  case 2 then //show
    c.visible="on"
    c.children.visible="on"
    g.axes_bounds=[0 0 1 2/3]
  case 3 then //adjust
    delete(c.children)
    delete(c)
    SA_gui(fig)
    draw(fig)
  end
endfunction
