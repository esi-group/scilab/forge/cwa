function class = classify(Sample,training,Group,Type)
  if argn(2)<4 then Type="mahalanobis";end
  if and(size(Group)<>1) then 
    error('Requires the third argument to be a vector.');
  end
  Group=Group(:);
  gr=size(Group,'*')

  if or(Group - round(Group)) | or(Group < 1) then
    error('The third input argument must be positive integers.');
  end
  maxg=max(Group);

  [tr,tc]=size(training);
  if tr<>gr then
    error('The number of rows in the second and third input arguments must match.');
  end

  [sr,sc]=size(Sample);
  if sc<>tc then
    error('The number of columns in the first and second input arguments must match.');
  end
  d=zeros(sr,maxg);
  select Type
  case "linear"
    //a single covarinace matrix for all classes
    gmeans=zeros(maxg,tc);
    for k=1:maxg
      gmeans(k,:) = mean(training(Group==k,:),1);
    end
    [Q,R] = qr((training - gmeans(Group,:))/sqrt(gr-maxg), "e");
    s = svd(R);
    for k=1:maxg
      w=(Sample-ones(sr,1)*gmeans(k,:))/R;
      d(:,k)=-(sum(w.^2,2)/2+sum(log(s)))
    end
  case "diaglinear"
    //a single covarinace matrix for all classes
    gmeans=zeros(maxg,tc);
    for k=1:maxg
      gmeans(k,:) = mean(training(Group==k,:),1);
    end
    s = stdev(training - gmeans(Group,:),1) * sqrt((gr-1)./(gr-maxg));
    for k=1:maxg
      w=(Sample-ones(sr,1)*gmeans(k,:))./(ones(sr,1)*s);
      d(:,k)=-sum(w.^2,2)/2-sum(log(s));
    end
  case "mahalanobis"
    //covariance of each class can be different
    for k=1:maxg
      Groupk=training(find(Group == k),:);
      d(:,k)=-mahal(Sample,Groupk);
    end
  case "quadratic"
    //covariance of each class can be different
    for k=1:maxg
      Groupk=training(find(Group == k),:);
      [d(:,k),Rk]=mahal(Sample,Groupk);
      s=svd(Rk.Rfact)
      d(:,k)=-d(:,k)/2-sum(log(s));
    end
  case "diagquadratic"
    //covariance of each class can be different
    for k=1:maxg
      Groupk=training(find(Group == k),:);
      s=stdev(Groupk,1);
      w=(Sample-ones(sr,1)*mean(Groupk,1))./(ones(sr,1)*s)
      d(:,k)=-sum(w.^2,2)-sum(log(s));
    end
  end
  [tmp, class]=max(d,'c');
endfunction
